<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\product\ProductController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/v2', 'App\Http\Controllers\HomeController@index')->name('home');
Route::get('/', 'App\Http\Controllers\HomeController@v2')->name('v2');

//ajax call
Route::prefix('ajax')->group(function(){
    Route::get('/get-citylist/{id}', 'App\Http\Controllers\AjaxController@getregionwisecity')->name('get.city');
    Route::get('/get-subpro/{id1}', 'App\Http\Controllers\AjaxController@GetProWiseSubpro')->name('get.subpro');
});

Route::get('/register','App\Http\Controllers\customer\RegisterController@create')->name('user.create');
//customer register
Route::prefix('user')->group(function(){
    Route::post('/create','App\Http\Controllers\customer\RegisterController@store')->name('user.create');
   Route::post('/store','App\Http\Controllers\customer\RegisterController@store')->name('user.store');
});

//user login
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

//products
Route::get("products",[ProductController::class,'list']);

//admin
Route::prefix('admin')->group(function(){
   Route::get('/login','App\Http\Controllers\Auth\AdminLoginController@ShowLoginForm')->name('admin.login');
   Route::post('/login','App\Http\Controllers\Auth\AdminLoginController@Login')->name('admin.login.submit');
   Route::get('/', 'App\Http\Controllers\admin\DashboardController@index')->name('admin.dashboard');	
   Route::get('/logout', 'App\Http\Controllers\Auth\AdminLoginController@logout')->name('admin.logout');
   Route::POST('/password/email','App\Http\Controllers\Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
   Route::GET('/password/reset','App\Http\Controllers\Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request'); 
   Route::POST('/password/reset', 'App\Http\Controllers\Auth\AdminResetPasswordController@reset');
   Route::GET('/password/reset/{token}','App\Http\Controllers\Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
});

//admin user management
Route::prefix('admin/user')->group(function(){
   Route::get('/adduser','App\Http\Controllers\admin\user\Usercontroller@index')->name('user.add');
   Route::post('/saveuser','App\Http\Controllers\admin\user\Usercontroller@save')->name('user.save');
   Route::get('/viewuser','App\Http\Controllers\admin\user\Usercontroller@view')->name('user.view');
   Route::get('/edituser/{id}','App\Http\Controllers\admin\user\Usercontroller@edit')->name('user.edit');
   Route::post('/updateuser', 'App\Http\Controllers\admin\user\Usercontroller@update')->name('user.update');
   Route::get('/blockuser/{id2}', 'App\Http\Controllers\admin\user\Usercontroller@block')->name('user.block');
   Route::get('/unblockuser/{id2}', 'App\Http\Controllers\admin\user\Usercontroller@Unblock')->name('user.unblock');
});

//admin role management
Route::prefix('admin/role')->group(function(){
   Route::get('/addrole','App\Http\Controllers\admin\role\Rolecontroller@index')->name('role.add');
   Route::post('/saverole','App\Http\Controllers\admin\role\Rolecontroller@save')->name('role.save');
   Route::get('/viewrole','App\Http\Controllers\admin\role\Rolecontroller@view')->name('role.view');
   Route::get('/editrole/{id}','App\Http\Controllers\admin\role\Rolecontroller@edit')->name('role.edit');
   Route::post('/updaterole/{id}', 'App\Http\Controllers\admin\role\Rolecontroller@update')->name('role.update');
   Route::get('/roledelete/{id}','App\Http\Controllers\admin\role\Rolecontroller@delete')->name('role.delete');
});

//admin product
Route::prefix('admin/product')->group(function(){
    Route::get('/addproduct','App\Http\Controllers\admin\product\ProductController@create')->name('product.add');
    Route::post('/saveproduct','App\Http\Controllers\admin\product\ProductController@store')->name('product.save');
    Route::get('/productlist','App\Http\Controllers\admin\product\ProductController@list')->name('product.list');
});
