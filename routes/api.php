<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\UserController;
use App\Http\Controllers\api\LocationController;
use App\Http\Controllers\api\customer\RegisterController;
use App\Http\Controllers\api\customer\LoginController;
use App\Http\Controllers\api\agent\AgentController;
use App\Http\Controllers\api\product\ProductController;
use App\Http\Controllers\api\product\ProductPriceController;
use App\Http\Controllers\api\product\ProductStock;
use App\Http\Controllers\api\product\ProductImage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post("login",[UserController::class,'index']);
Route::post("customer/login",[LoginController::class,'login']);
Route::get("city-list/{id}",[LocationController::class,'getcity']);
Route::post("register",[RegisterController::class,'store']);
Route::get("agents",[AgentController::class,'list']);

//products info
Route::get("products",[ProductController::class,'productinfo']);
Route::get("products-price",[ProductPriceController::class,'index']);
Route::get("products-stock",[ProductStock::class,'index']);
Route::get("products-album",[ProductImage::class,'album']);
Route::get("products-image",[ProductImage::class,'images']);

Route::group(['middleware' => 'auth:sanctum'], function(){
   Route::get("regions",[LocationController::class,'getregion']);
   
});