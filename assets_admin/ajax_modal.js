function confirm_delete(delete_url) {
    jQuery("#modal-delete").modal('show', {backdrop: 'static'});
    document.getElementById("delete_link").setAttribute('href', delete_url);
}
function confirm_clear_cart() {
    jQuery("#modal-delete").modal('show', {backdrop: 'static'});
}
function validate(form) {
    // validation code here ...
    if (!form) {
        alert('Please correct the errors in the form!');
        return false;
    } else {
        return confirm('Do you really want to clear shopping cart');
    }
}