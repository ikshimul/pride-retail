<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userdetails', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->bigIncrements('id');
			$table->bigInteger('user_id')->unsigned();
			$table->string('tarde_license');
			$table->string('tarde_license_pdf');
			$table->string('nid_number');
			$table->string('nid_pdf');
			$table->string('phone');
			$table->string('address');
			$table->string('country')->nullable();
			$table->bigInteger('region')->unsigned();
			$table->bigInteger('city')->unsigned();
			$table->string('zip')->nullable();
			$table->ipAddress('user_ip')->nullable();
            $table->timestamps();
			
			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('region')->references('id')->on('regions');
			$table->foreign('city')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userdetails');
    }
}
