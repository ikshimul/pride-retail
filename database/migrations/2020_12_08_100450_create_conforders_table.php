<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conforders', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->bigIncrements('id');
			$table->bigInteger('user_id')->unsigned();	 
			$table->bigInteger('shoppingcart_id')->unsigned();
			$table->string('conforder_tracknumber');
			$table->string('conforder_status');
			$table->string('order_threepldlv')->nullable();
			$table->string('conforder_statusdetails')->nullable();
			$table->date('conforder_placed_date')->nullable();
			$table->date('conforder_deliverydate')->nullable();
			$table->date('pos_entry_date')->nullable();
			$table->string('conforder_deliverynotes')->nullable();
			$table->integer('delivery_by')->nullable();			
            $table->timestamps();
			
			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('shoppingcart_id')->references('id')->on('shoppingcarts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conforders');
    }
}
