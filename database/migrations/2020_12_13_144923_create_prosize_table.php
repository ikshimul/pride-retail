<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProsizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('prosizes', function (Blueprint $table) {
            $table->id();
            $table->integer('procat_id')->unsigned();
			$table->integer('subprocat_id')->unsigned();
			$table->string('sizename');
            $table->timestamps();
            
            $table->foreign('procat_id')->references('id')->on('procats');
			$table->foreign('subprocat_id')->references('id')->on('subprocats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prosize');
    }
}
