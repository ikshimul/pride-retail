<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShoppingcartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shoppingcarts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
			$table->integer('voucher_id')->unsigned();
			$table->integer('shipping_charge');
			$table->integer('extra_charge')->default(0);
			$table->mediumInteger('shoppingcart_vat')->nullable();
			$table->mediumInteger('shoppingcart_subtotal');
			$table->mediumInteger('shoppingcart_discount')->default(0);
			$table->mediumInteger('shoppingcart_total');
			$table->string('payment_method');
			$table->bigInteger('shipping_area')->unsigned();
            $table->timestamps();
			
			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('shipping_area')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shoppingcarts');
    }
}
