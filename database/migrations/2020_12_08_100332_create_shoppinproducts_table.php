<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShoppinproductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shoppinproducts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->bigIncrements('id');
			$table->bigInteger('shoppingcart_id')->unsigned();
			$table->bigInteger('product_id')->unsigned();
			$table->string('product_barcode');
			$table->string('prosize_name');
			$table->string('productalbum_name');
			$table->integer('product_price');
			$table->integer('shoppinproduct_quantity');
			$table->string('cart_image');
            $table->timestamps();
			
			$table->foreign('shoppingcart_id')->references('id')->on('shoppingcarts');
			$table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shoppinproducts');
    }
}
