<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->bigIncrements('id');
			$table->integer('procat_id')->unsigned();
			$table->integer('subprocat_id')->unsigned();
			$table->string('product_styleref');
			$table->string('product_name');
			$table->string('fabric')->nullable();
			$table->string('product_description');
			$table->string('product_img_thm')->nullable();
			$table->string('product_care')->nullable();
			$table->tinyInteger('product_active_deactive')->default(0);
			$table->tinyInteger('isSpecial')->default(0)->comment('2 for Eid Collection,3 for puja 18,4 for falgun 19,5 for Amar Ekushay 2019,6 for Independence day,7 for Boishak,8 summer,9 Capsule,10 popart,11 zodiac');
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->tinyInteger('trash')->default(0);
			$table->Integer('sale')->default(0);
			$table->Integer('view')->default(0);
            $table->timestamps();
			$table->dateTime('deleted_at')->nullable();
			
			$table->foreign('procat_id')->references('id')->on('procats');
			$table->foreign('subprocat_id')->references('id')->on('subprocats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
