@extends('admin.layouts.app')
@section('title', 'Pride Limited')
@section('content')
<!-- Content Header (Page header) -->
<style>
    .label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 600;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
}
 @-webkit-keyframes blinker {
    from {opacity: 1.0;}
    to {opacity: 0.0;}
}
.blink{
    color: red;
    font-weight:bold;
    text-decoration: blink;
    -webkit-animation-name: blinker;
    -webkit-animation-duration: 0.8s;
    -webkit-animation-iteration-count:infinite;
    -webkit-animation-timing-function:ease-in-out;
    -webkit-animation-direction: alternate;
}
.products-list .product-img img {
    width: 50px;
    height: 75px;
}
</style>
<section class="content-header">
    <h1>
        Dashboard 
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
<section id="auth-button"></section>
<section id="view-selector"></section>
<section id="timeline"></section>
</section>
<script>
    $(document).ready(function () {
    var url_op = base_url + "/pride-admin/getComprisonChartData";
		$.ajax({
			url: url_op,
			type: 'GET',
			dataType: 'json',
			data: '',
			success: function (response)
			{
				//  alert(response);
				  getComprisonChartData(response);
			}
		});
     var url_op = base_url + "/pride-admin/gebrowserdata";
        $.ajax({
            url: url_op,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (response)
            {
                browserData(response);
            }
        });
        
       
    var sdyear =   $("#SelectedYear option:selected").val(); 
    $("#SelectedYear").change(function(){
     sdyear =   $("#SelectedYear").val(); 
        var url_op1 = base_url + "/pride-admin/getmonthlysalesdata/"+sdyear;
        $.ajax({
            url: url_op1,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (response)
            {
               monthlysalesData(response,sdyear);
            }
        }); 
     });
     var url_op1 = base_url + "/pride-admin/getmonthlysalesdata/"+sdyear;
        $.ajax({
            url: url_op1,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (response)
            {
               // alert(response);
               monthlysalesData(response,sdyear);
            }
        }); 
        
        
        
        
    });
    


    function browserData(response) {
        var pieColors = (function () {
            var colors = [],
                    base = Highcharts.getOptions().colors[0],
                    i;
            for (i = 0; i < 10; i += 1) {
                colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
            }
            return colors;
        }());
        Highcharts.chart('container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Browsers use in Pride Limited website, 2018'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    colors: pieColors,
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                        distance: -50,
                        filter: {
                            property: 'percentage',
                            operator: '>',
                            value: 4
                        }
                    }
                }
            },
            series: [{
                    name: 'Browser',
                    data: response
                }]
        });

    }
    
    
    
    function  monthlysalesData(response,sdyear){

        var chart = Highcharts.chart('linechart_container', {
            title: {
                text: 'Monthly Sales Graph:' + sdyear
            },
            // subtitle: {
            //     text: 'Monthly Sales Graph:' + sdyear
            // },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May','Jun','July','Aug','Sep','Oct','Nov','Dec']
            },
            series: [{
                    type: 'column',
                    colorByPoint: true,
                    data:  $.parseJSON(response),
                    showInLegend: false
                }]

        });


        $('#plain').click(function () {
            chart.update({
                chart: {
                    inverted: false,
                    polar: false
                },
                subtitle: {
                    text: 'Plain'
                }
            });
        });

        $('#inverted').click(function () {
            chart.update({
                chart: {
                    inverted: true,
                    polar: false
                },
                subtitle: {
                    text: 'Inverted'
                }
            });
        });

        $('#polar').click(function () {
            chart.update({
                chart: {
                    inverted: false,
                    polar: true
                },
                subtitle: {
                    text: 'Polar'
                }
            });
        });
      
     }
     
     function getComprisonChartData(response){
		
		console.log(response);
		
		 Highcharts.chart('yearlysellcomprisoncontainer', {
		chart: {
			type: 'line'
		},
		title: {
			text: 'Yearly Sales Comprison Report'
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		},
		yAxis: {
			title: {
				text: 'TK (BDT)'
			}
		},
		plotOptions: {
			line: {
				dataLabels: {
					enabled: true
				},
				enableMouseTracking: false
			}
		},
		series:  response.Series
	});  
							
	}
</script>               
@endsection
