<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        <link rel="icon" type="image/png" href="{{asset('assets_admin/images/logo.png')}}">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="{{asset('assets_admin/bootstrap/css/bootstrap.min.css')}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

        <link rel="stylesheet" href="{{asset('assets_admin/plugins/iCheck/all.css')}}">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/colorpicker/bootstrap-colorpicker.min.css')}}">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/timepicker/bootstrap-timepicker.min.css')}}">
        <!-- Select2 -->
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/select2/select2.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/datatables/dataTables.bootstrap.css')}}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('assets_admin/dist/css/fileinput.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/dist/css/AdminLTE.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/dist/css/fileinput.min.css')}}">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{asset('assets_admin/dist/css/skins/_all-skins.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/pace/pace.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/morris/morris.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
        <!-- daterange picker -->
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/daterangepicker/daterangepicker.css')}}">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/datepicker/datepicker3.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/jquery-ui.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/toastr.css')}}" />

        <script src="{{asset('assets_admin/highcharts.js')}}"></script>
        <script src="{{asset('assets_admin/highcharts-more.js')}}"></script>
        <script src="{{asset('assets_admin/exporting.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/jQuery/jquery-3.1.1.min.js')}}"></script>
        
       <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script> -->

        <!-- iCheck for checkboxes and radio inputs -->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <script>
var base_url = "{{ URL::to('') }}";
var csrf_token = "{{ csrf_token() }}";
        </script>
        <style>
            .bv-form .help-block {
                margin-bottom: 0;
            }
            .bv-form .tooltip-inner {
                text-align: left;
            }
            .nav-tabs li.bv-tab-success > a {
                color: #3c763d;
            }
            .nav-tabs li.bv-tab-error > a {
                color: #a94442;
            }

            .bv-form .bv-icon-no-label {
                top: 0;
            }

            .bv-form .bv-icon-input-group {
                top: 0;
                z-index: 100;
            }
            .error{
                color: red;
                padding: 5px;
            }
            .loading-modal {
            display:    none;
            position:   fixed;
            z-index:    1000;
            top:        0;
            left:       0;
            height:     100%;
            width:      100%;
            background: rgba( 255, 255, 255, .8 ) 
                        url("{{url('/')}}/storage/app/public/loading.gif") 
                        50% 50% 
                        no-repeat;
        }
        button.toast-close-button {
            height: 0px;
        }
        .skin-yellow-light .main-header .logo {
            background-color: #f39c12c9;
        }
        
        span.logo-lg{
            position: relative;
            top: -2px;
            left: 0%;
           }
        img.logo-tab{
               width: 42%;
           }
        @media only screen and (max-width: 600px) {
          span.logo-lg{
            position: relative;
            top: 0px;
            left: 0%;
           }
           img.logo-tab{
               width: 27%;
           }
        }
        .bg-gray {
            color: white;
            background-color: #3b3092 !important;
        }
        </style>
        <script>
            (function(w,d,s,g,js,fs){
              g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
              js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
              js.src='https://apis.google.com/js/platform.js';
              fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
            }(window,document,'script'));
       </script>
    </head>
    <?php
      use App\Http\Controllers\admin\DashboardController;
      $permission = DashboardController::getRolePermission();
    ?>
    <!--hold-transition skin-blue sidebar-mini -->
    <body ng-app="crudApp" ng-controller="crudController" class="sidebar-mini skin-yellow-light">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="{{url('/pride-admin')}}" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>PRI</b>DE</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><img class="logo-tab" src="{{url('/')}}/storage/app/public/logo_new.png"/></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                            <li>
                                <a href="#" id="datasync"> Data Sync <i class="fa fa-refresh" aria-hidden="true"></i></a>
                            </li>
                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-phone"></i>
                                    <span class="label label-danger"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 0 phone request(s)</li>
                                    <li>
                                        
                                    </li>
                                    <li class="footer">
                                        <a href="#">See All Phone request(s)</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- Notifications: style can be found in dropdown.less -->
                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-cart-plus"></i>
                                    <span class="label label-danger">0</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 0 new order</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                  
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#"> Go to incomplete order page</a></li>
                                </ul>
                            </li>
                            <!-- Tasks: style can be found in dropdown.less -->
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="{{ URL::to('') }}/storage/app/public/admin/user/{{ Auth::guard('admin')->user()->image }}" class="user-image" alt="User Image">
                                    <span class="hidden-xs">{{ Auth::guard('admin')->user()->name }}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="{{ URL::to('') }}/storage/app/public/admin/user/{{ Auth::guard('admin')->user()->image }}" class="img-circle" alt="User Image">
                                        <p>
                                            {{ Auth::guard('admin')->user()->name }}
                                            <small>Since <?php $join=strtotime(Auth::guard('admin')->user()->created_at); 
                                            echo date('d M , Y',$join);?></small>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="{{url('/admin-profile')}}" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="{{url('/admin/logout')}}" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button 
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>-->
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{ URL::to('') }}/storage/app/public/admin/user/{{ Auth::guard('admin')->user()->image }}" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>{{ Auth::guard('admin')->user()->name }}</p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <br>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header"></li>
                        <li class="{{ Request::is('admin') ? 'active' : '' }} treeview">
						  <a href="#">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span>
							<span class="pull-right-container">
							  <i class="fa fa-angle-left pull-right"></i>
							</span>
						  </a>
						  <ul class="treeview-menu">
							<li class="{{ Request::is('admin') ? 'active' : '' }}"><a href="{{url('/admin')}}"><i class="fa fa-circle-o"></i> Dashboard</a></li>
						  </ul>
						</li>
						
                        <!--<li class="{{ Request::is('manage-category') ? 'active' : '' }}{{ Request::is('add-subcat') ? 'active' : '' }}{{ Request::is('manage-subpro') ? 'active' : '' }} treeview">-->
                        <!--    <a href="#">-->
                        <!--        <i class="fa fa-list" aria-hidden="true"></i> <span>Category</span>-->
                        <!--        <span class="pull-right-container">-->
                        <!--            <i class="fa fa-angle-left pull-right"></i>-->
                        <!--        </span>-->
                        <!--    </a>-->
                        <!--    <ul class="treeview-menu">-->
                        <!--        <li class="{{ Request::is('manage-category') ? 'active' : '' }}"><a href="{{url('/manage-category')}}"><i class="fa fa-circle-o"></i> Manage Category</a></li>-->
                        <!--        <li class="{{ Request::is('add-subcat') ? 'active' : '' }}"><a href="{{url('/add-subcat')}}"><i class="fa fa-circle-o"></i> Add New Sub Category</a></li>-->
                        <!--        <li class="{{ Request::is('manage-subpro') ? 'active' : '' }}"><a href="{{url('/manage-subpro')}}"><i class="fa fa-circle-o"></i> Browse All Sub Category</a></li>-->
                        <!--    </ul>-->
                        <!--</li>-->
                        
                        <?php if (in_array('product-menu', $permission)) { ?>
                        <li class="{{ Request::is('admin/product/addproduct') ? 'active' : '' }}{{ Request::is('admin/product/productlist') ? 'active' : '' }} treeview">
                            <a href="#">
                                <i class="fa fa-product-hunt" aria-hidden="true"></i>
                                <span>Product</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('admin/product/addproduct') ? 'active' : '' }}"><a href="{{url('admin/product/addproduct')}}"><i class="fa fa-circle-o"></i> Add Product</a></li>
                                <li class="{{ Request::is('admin/product/productlist') ? 'active' : '' }}"><a href="{{url('admin/product/productlist')}}"><i class="fa fa-circle-o"></i> Manage Product</a></li>
                                
                            </ul>
                        </li>
                        <?php } ?>
                        
                        
                        <!--<li class="{{ Request::is('admin/order/incomplete') ? 'active' : '' }}{{ Request::is('manage-all-order') ? 'active' : '' }}{{ Request::is('order-report') ? 'active' : '' }}{{ Request::is('search-order') ? 'active' : '' }}{{ Request::is('all-order') ? 'active' : '' }} treeview">-->
                        <!--    <a href="#">-->
                        <!--        <i class="fa fa-shopping-cart" aria-hidden="true"></i>-->
                        <!--        <span>Manage Order</span>-->
                        <!--        <span class="pull-right-container">-->
                        <!--            <i class="fa fa-angle-left pull-right"></i>-->
                        <!--            <small class="label pull-right bg-red">-->
                        <!--              0-->
                        <!--            </small>-->
                        <!--        </span>-->
                        <!--    </a>-->
                        <!--    <ul class="treeview-menu">-->
                        <!--        <li class="{{ Request::is('admin/order/incomplete') ? 'active' : '' }}"><a href="{{url('admin/order/incomplete')}}"><i class="fa fa-circle-o"></i> Incomplete Order 	-->
                        <!--                <span class="pull-right-container">-->
                        <!--                    <small class="label pull-right bg-blue">0</small>-->
                        <!--                </span></a></li>-->
                        <!--        <li class="{{ Request::is('all-order') ? 'active' : '' }}"><a href="{{url('all-order')}}"><i class="fa fa-circle-o"></i> All Order</a></li>-->
                        <!--    </ul>-->
                        <!--</li>-->
                      
                        <!--<li class="{{ Request::is('add-slider') ? 'active' : '' }}{{ Request::is('manage-slider') ? 'active' : '' }}{{ Request::is('add-banner') ? 'active' : '' }}{{ Request::is('manage-banner') ? 'active' : '' }}{{ Request::is('add-popup') ? 'active' : '' }}{{ Request::is('manage-popup') ? 'active' : '' }} treeview">-->
                        <!--    <a href="#">-->
                        <!--        <i class="fa fa-home" aria-hidden="true"></i> <span>Manage Home Page</span>-->
                        <!--        <span class="pull-right-container">-->
                        <!--            <i class="fa fa-angle-left pull-right"></i>-->
                        <!--        </span>-->
                        <!--    </a>-->
                        <!--    <ul class="treeview-menu">-->
                        <!--        <li class="{{ Request::is('add-slider') ? 'active' : '' }}{{ Request::is('manage-slider') ? 'active' : '' }} treeview">-->
                        <!--            <a href="#"><i class="fa fa-circle-o"></i> Slider-->
                        <!--                <span class="pull-right-container">-->
                        <!--                    <i class="fa fa-angle-left pull-right"></i>-->
                        <!--                </span>-->
                        <!--            </a>-->
                        <!--            <ul class="treeview-menu">-->
                        <!--                <li class="{{ Request::is('add-slider') ? 'active' : '' }}"><a href="{{url('/add-slider')}}"><i class="fa fa-circle-o"></i> Add Slider</a></li>-->
                        <!--                <li class="{{ Request::is('manage-slider') ? 'active' : '' }}"><a href="{{url('/manage-slider')}}"><i class="fa fa-circle-o"></i> Manage Slider</a></li>-->
                        <!--            </ul>-->
                        <!--        </li>-->
                        <!--        <li class="{{ Request::is('add-banner') ? 'active' : '' }}{{ Request::is('manage-banner') ? 'active' : '' }} treeview">-->
                        <!--            <a href="#"><i class="fa fa-circle-o"></i> Banner-->
                        <!--                <span class="pull-right-container">-->
                        <!--                    <i class="fa fa-angle-left pull-right"></i>-->
                        <!--                </span>-->
                        <!--            </a>-->
                        <!--            <ul class="treeview-menu">-->
                        <!--                <li class="{{ Request::is('add-banner') ? 'active' : '' }}"><a href="{{url('/add-banner')}}"><i class="fa fa-circle-o"></i> Add Banner</a></li>-->
                        <!--                <li class="{{ Request::is('manage-banner') ? 'active' : '' }}"><a href="{{url('/manage-banner')}}"><i class="fa fa-circle-o"></i> Manage Banner</a></li>-->
                        <!--            </ul>-->
                        <!--        </li>-->
                        <!--        <li class="{{ Request::is('add-popup') ? 'active' : '' }}{{ Request::is('manage-popup') ? 'active' : '' }} treeview">-->
                        <!--            <a href="#"><i class="fa fa-circle-o"></i> Popup-->
                        <!--                <span class="pull-right-container">-->
                        <!--                    <i class="fa fa-angle-left pull-right"></i>-->
                        <!--                </span>-->
                        <!--            </a>-->
                        <!--            <ul class="treeview-menu">-->
                        <!--                <li class="{{ Request::is('add-popup') ? 'active' : '' }}"><a href="{{url('/add-popup')}}"><i class="fa fa-circle-o"></i> Add Popup</a></li>-->
                        <!--                <li class="{{ Request::is('manage-popup') ? 'active' : '' }}"><a href="{{url('/manage-popup')}}"><i class="fa fa-circle-o"></i> Manage Popup</a></li>-->
                        <!--            </ul>-->
                        <!--        </li>-->
                        <!--    </ul>-->
                        <!--</li>-->
                            
                        <?php if (in_array('user-menu', $permission)) { ?>
                        <li class="{{ Request::is('admin/user/adduser') ? 'active' : '' }}{{ Request::is('admin/user/viewuser') ? 'active' : '' }}{{ Request::is('admin/role/addrole') ? 'active' : '' }}{{ Request::is('admin/role/viewrole') ? 'active' : '' }} treeview">
                            <a href="#">
                                <i class="fa fa-user"></i> <span>Admin User</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('admin/user/adduser') ? 'active' : '' }}{{ Request::is('admin/user/viewuser') ? 'active' : '' }} treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i> Manage User
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li class="{{ Request::is('admin/user/adduser') ? 'active' : '' }}"><a href="{{url('/admin/user/adduser')}}"><i class="fa fa-circle-o"></i> Add User</a></li>
                                        <li class="{{ Request::is('admin/user/viewuser') ? 'active' : '' }}"><a href="{{url('admin/user/viewuser')}}"><i class="fa fa-circle-o"></i> View User</a></li>
                                    </ul>
                                </li>
                                <li class="{{ Request::is('admin/role/addrole') ? 'active' : '' }}{{ Request::is('admin/role/viewrole') ? 'active' : '' }} treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i> Manage Permission Role
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li class="{{ Request::is('admin/role/addrole') ? 'active' : '' }}"><a href="{{url('admin/role/addrole')}}"><i class="fa fa-circle-o"></i> Add Role</a></li>
                                        <li class="{{ Request::is('admin/role/viewrole') ? 'active' : '' }}"><a href="{{url('admin/role/viewrole')}}"><i class="fa fa-circle-o"></i> View Role</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                       <?php } ?>
                        <!--<li class="{{ Request::is('site-user') ? 'active' : '' }}{{ Request::is('customer-send-message-form') ? 'active' : '' }}{{ Request::is('customer-send-sms') ? 'active' : '' }} treeview">-->
                        <!--    <a href="#">-->
                        <!--        <i class="fa fa-users" aria-hidden="true"></i>-->
                        <!--        <span>Manage Customer</span>-->
                        <!--        <span class="pull-right-container">-->
                        <!--            <i class="fa fa-angle-left pull-right"></i>-->
                                    <!--<small class="label pull-right bg-red">
                        <!--                </small> --->
                        <!--        </span>-->
                        <!--    </a>-->
                        <!--    <ul class="treeview-menu">-->
                        <!--        <li class="{{ Request::is('site-user') ? 'active' : '' }}"><a href="{{url('/site-user')}}"><i class="fa fa-circle-o"></i>Customer List	-->
                        <!--            <span class="pull-right-container">-->
                        <!--                <small class="label pull-right bg-blue"></small>-->
                        <!--            </span>-->
                        <!--        </a></li>-->
                        <!--        <li class="{{ Request::is('customer-send-message-form') ? 'active' : '' }}"><a href="{{url('/customer-send-message-form')}}"><i class="fa fa-circle-o"></i>Send Email</a></li>-->
                        <!--        <li class="{{ Request::is('customer-send-sms') ? 'active' : '' }}"><a href="{{url('/customer-send-sms')}}"><i class="fa fa-circle-o"></i>Send SMS</a></li>-->
                        <!--    </ul>-->
                        <!--</li>-->
                        
                        <li class="{{ Request::is('site-user') ? 'active' : '' }}{{ Request::is('customer-send-message-form') ? 'active' : '' }}{{ Request::is('customer-send-sms') ? 'active' : '' }} treeview">
                            <a href="#">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span>Manage Agents</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                    <!--<small class="label pull-right bg-red">
                                        </small> --->
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('site-user') ? 'active' : '' }}"><a href="{{url('/site-user')}}"><i class="fa fa-circle-o"></i>Agents List	
                                    <span class="pull-right-container">
                                        <small class="label pull-right bg-blue"></small>
                                    </span>
                                </a></li>
                            </ul>
                        </li>

                        <!--<li class="{{ Request::is('sale-reports') ? 'active' : '' }}{{ Request::is('custom-sale-reports') ? 'active' : '' }} treeview">-->
                        <!--    <a href="#">-->
                        <!--        <i class="fa fa-bar-chart" aria-hidden="true"></i>-->
                        <!--        <span>Reports</span>-->
                        <!--        <span class="pull-right-container">-->
                        <!--            <i class="fa fa-angle-left pull-right"></i>-->
                                    <!--<small class="label pull-right bg-red">
                        <!--                </small> --->
                        <!--        </span>-->
                        <!--    </a>-->
                        <!--    <ul class="treeview-menu">-->
                        <!--        <li class="{{ Request::is('sale-reports') ? 'active' : '' }}"><a href="#"><i class="fa fa-circle-o"></i>Sales Report	-->
                        <!--            <span class="pull-right-container">-->
                        <!--                <small class="label pull-right bg-blue"></small>-->
                        <!--            </span>-->
                        <!--        </a>-->
                        <!--        </li>-->
                        <!--        <li class="{{ Request::is('custom-sale-reports') ? 'active' : '' }}"><a href="#"><i class="fa fa-circle-o"></i>Custom Report	-->
                        <!--            <span class="pull-right-container">-->
                        <!--                <small class="label pull-right bg-blue"></small>-->
                        <!--            </span>-->
                        <!--        </a>-->
                        <!--        </li>-->
                        <!--    </ul>-->
                        <!--</li>-->

                        
                        <li class="header"></li>
                       <li class="{{ Request::is('admin-profile') ? 'active' : '' }}"><a href="{{url('/admin-profile')}}"><i class="fa fa-circle-o text-red"></i> <span>Profile Manage</span></a></li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Cont                                                                                                                                                                                                        ent Wrapper. Contai                                                                                                                                                                                                        ns page content -->
            <div class="content-wrapper">
                <div class="loading-modal"></div>
                <!-- Main content -->
                @yield('content')
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Laravel Version</b> {{ App::VERSION() }}
                </div>
                <strong>Copyright &copy; <?php echo date('Y');?> <a href="#">Pride Retail</a>.</strong> All rights
                reserved.
            </footer>
        </div>

        <!--<script src="{{asset('assets_admin/jquery-ui.min.js')}}"></script>-->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
$.widget.bridge('uibutton', $.ui.button);</script>
        <script src="{{asset('assets_admin/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/pace/pace.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/select2/select2.full.min.js')}}"></script>
        <!-- InputMask -->
        <script src="{{asset('assets_admin/plugins/input-mask/jquery.inputmask.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
        <script src="{{asset('assets_admin/raphael-min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/morris/morris.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/knob/jquery.knob.js')}}"></script>
        <script src="{{asset('assets_admin/moment.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/daterangepicker/daterangepicker.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/fastclick/fastclick.js')}}"></script>
        <script src="{{asset('assets_admin/dist/js/adminlte.min.js')}}"></script>
        <script src="{{asset('assets_admin/dist/js/fileinput.min.js')}}"></script>
        <script src="{{asset('assets_admin/dist/js/pages/dashboard.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/chartjs/Chart.min.js')}}"></script>
        <script src="{{asset('assets_admin/dist/js/pages/dashboard2.js')}}"></script>
        <script src="{{asset('assets_admin/dist/js/demo.js')}}"></script>
        <script src="{{asset('assets_admin/dist/js/form_validation.js')}}"></script>
        <script src="{{asset('assets_admin/ajax_modal.js')}}"></script>
        <script src="{{ asset('assets_admin/custom-script.js') }}?4"></script>
        <script src="{{asset('assets/js/toastr.js')}}"></script>
        <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false
            });
            $("#datasync").click(function(){
             //   alert('ok');
             var url = base_url + "/save-item-list/";
             $(".loading-modal").show();
             $.ajax({
                 url:url,
                 type: 'GET',
                 success: function (data) {
                    $(".loading-modal").hide();
                    alert(data); 
                    location.reload();
                 }
               });
            });
        });
        </script>
        <script>
            $(function () {
                //Initialize Select2 Elements
                $(".select2").select2();
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();

                //Date range picker
                $('#reservation').daterangepicker();
                //Date range picker with time picker
                $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                            },
                            startDate: moment().subtract(29, 'days'),
                            endDate: moment()
                        },
                        function (start, end) {
                            $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                        }
                );

                //Date picker
                $('#datepicker').datepicker({
                    autoclose: true
                });

                //iCheck for checkbox and radio inputs
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass: 'iradio_minimal-blue'
                });
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red',
                    radioClass: 'iradio_minimal-red'
                });
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });

                //Colorpicker
                $(".my-colorpicker1").colorpicker();
                //color picker with addon
                $(".my-colorpicker2").colorpicker();

                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false
                });
            });
        </script>
        @yield('appended.script')
        @toastr_render
    </body>
</html>
