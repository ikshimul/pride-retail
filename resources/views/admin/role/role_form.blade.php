@extends('admin.layouts.app')
@section('title', 'Add Role')
@section('content')
<style>
    .user_access{
        border: 1px solid #33333330;
        padding: 25px;
        margin-left: 5px;
    }
    label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 400;
  }
</style>
<section class="content">
    @if(session('save'))
    <div class="alert alert-success" role="alert">
        {{session('save')}}
    </div>
    @endif
    @if(session('error'))
    <div class="alert alert-danger" role="alert">
        {{session('error')}}
    </div>
    @endif
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Add New Role</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <form action="{{url('/admin/role/saverole')}}" method="post">
                    {{csrf_field()}}
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Role Name</label>
                            <input type="text" name="role_name" value="{{old('role_name')}}" class="form-control">
                            <span class="text-danger">{{ $errors->first('role_name') }}</span>
                        </div>
                    </div>
                    <div class="col-md-6">&nbsp;</div>
                    <div >
                        <div class="col-md-12">
                            <div class="col-md-12" style="font-weight: 700;">
                                <input type="checkbox" id="ckbCheckAll" /> Check All
                            </div>
                            <p class="title">&nbsp;</p>
                            <div class="col-md-3">
                                    <div class="user_access">
                                        <h4>Dashboard</h4>
                                        <div class="form-group">
                                            <label style="font-weight: 700;">
                                                <input type="checkbox" name="permission[]" value="admin" class="checkBoxClass flat-red">
                                                       Dashboard
                                            </label>
                                        </div>
                                    </div>
                                 </div>
                                 
                            
                            <div class="col-md-3">
                                    <div class="user_access">
                                      <h4>Admin User</h4>
                                      <input type="checkbox" id="AdminCheckAll" /> Check All
                                      <br>
                                        <div class="form-group">
                                            <label style="font-weight: 700;">
                                                <input type="checkbox" name="permission[]" value="user-menu" class="checkBoxClass flat-red checkBoxAdmin">
                                                       &nbsp;User Main Menu
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="adduser" class="checkBoxClass flat-red checkBoxAdmin">
                                                       &nbsp;Add User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="saveuser" class="checkBoxClass flat-red checkBoxAdmin">
                                                       &nbsp;Save User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="viewuser" class="checkBoxClass flat-red checkBoxAdmin">
                                                       &nbsp;View User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="edituser" class="checkBoxClass flat-red checkBoxAdmin">
                                                       &nbsp;Edit User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="updateuser" class="checkBoxClass flat-red checkBoxAdmin">
                                                       &nbsp;Update User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="blockuser" class="checkBoxClass flat-red checkBoxAdmin">
                                                       &nbsp;Block User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="unblockuser" class="checkBoxClass flat-red checkBoxAdmin">
                                                       &nbsp;Unblock User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="addrole" class="checkBoxClass flat-red checkBoxAdmin">
                                                       &nbsp;Add Role
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="saverole" class="checkBoxClass flat-red checkBoxAdmin">
                                                       &nbsp; Save Role
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="viewrole" class="checkBoxClass flat-red checkBoxAdmin">
                                                       &nbsp; View Role
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="editrole" class="checkBoxClass flat-red checkBoxAdmin">
                                                       &nbsp; View Role
                                            </label>
                                        </div>
                                        
                                        <hr>
                                        <h4>Admin Profile</h4>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="admin-profile" class="checkBoxClass flat-red checkBoxAdmin">
                                                       &nbsp;Profile
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="admin-change-password" class="checkBoxClass flat-red checkBoxAdmin">
                                                       &nbsp;Change Password
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <div class="user_access">
                                        <h4>Product</h4>
                                        <input type="checkbox" id="ProductCheckAll" /> Check All
                                        <br/><br/>
                                        <div class="form-group">
                                            <label style="font-weight: 700;">
                                                <input type="checkbox" name="permission[]" value="product-menu" class="checkBoxClass checkBoxproduct flat-red">
                                                   Product Main Menu
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="addproduct" class="checkBoxClass checkBoxproduct flat-red">
                                                Add Product
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="saveproduct" class="checkBoxClass checkBoxproduct flat-red">
                                                Product Save
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="productlist" class="checkBoxClass checkBoxproduct flat-red">
                                                Product Save
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="product-delete" class="checkBoxClass checkBoxproduct flat-red">
                                                Product Delete
                                            </label>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="box-footer">
            <input type="submit" name="btn" class="btn btn-success" value="Add Role">
        </div>
        <form>
    </div>
</section>  
<script>
    $(document).ready(function () {
        $("#ckbCheckAll").click(function () {
            $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        });

        $(".checkBoxClass").change(function () {
            if (!$(this).prop("checked")) {
                $("#ckbCheckAll").prop("checked", false);
            }
        });
        $("#AdminCheckAll").click(function () {
            $(".checkBoxAdmin").prop('checked', $(this).prop('checked'));
        });
        $("#CategoryCheckAll").click(function () {
            $(".checkBoxCategory").prop('checked', $(this).prop('checked'));
        });
        $("#ProductCheckAll").click(function(){
             $(".checkBoxproduct").prop('checked', $(this).prop('checked'));
        });
        $("#OrderCheckAll").click(function(){
             $(".checkBoxorder").prop('checked', $(this).prop('checked'));
        });
        $("#DashboardCheckAll").click(function(){
             $(".checkBoxDashboard").prop('checked', $(this).prop('checked'));
        });
    });
</script>
@stop