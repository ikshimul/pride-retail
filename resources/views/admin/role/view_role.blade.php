@extends('admin.layouts.app')
@section('title', 'Role List')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if(session('success_message'))
                <div class="alert alert-success" role="alert">
                    {{session('success_message')}}
                </div>
            @endif

            @if(session('error_message'))
                <div class="alert alert-danger" role="alert">
                    {{session('error_message')}}
                </div>
            @endif

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">View Role</h3>
                </div>

                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Role Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($role as $role_list)
                            <tr>
                                <td>{{$role_list->role_name}}</td>
                                <td>
                                    <a href="{{url('admin/role/editrole/'.$role_list->id)}}" class="btn btn-sm btn-primary">Edit</a>
                                    <a href="#" onClick="confirm_delete('{{url("admin/role/roledelete/{$role_list->id}")}}')" class="btn btn-sm btn-danger">Delete</a>
                                </td>
                            </tr>
							@endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Role Name</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Ajax modal ---->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to delete this album ?</h4>
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="#" class="btn btn-sm btn-danger" id="delete_link">Delete</a>
                <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--- Ajax modal end ---->
@stop