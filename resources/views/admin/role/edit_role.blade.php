@extends('admin.layouts.app')
@section('title', 'Role List')
@section('content')
<style>
    .user_access{
        border: 1px solid #33333330;
        padding: 25px;
        margin-left: 5px;
    }
    label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 400;
  }
</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if(session('update'))
            <div class="alert alert-success" role="alert">
                {{session('update')}}
            </div>
            @endif
            @if(session('error_message'))
            <div class="alert alert-danger" role="alert">
                {{session('error_message')}}
            </div>
            @endif

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Role</h3>
                </div>
                <form role="form" action="{{url('admin/role/updaterole/'.$role->id)}}" method="post">
                    {{csrf_field()}}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label >Role Name</label>
                                    <input type="text" name="role_name" value="{{$role->role_name}}" class="form-control" placeholder="Enter Role Name">
                                    <span class="text-danger">{{$errors->first('role_name')}}</span>
                                </div>
                            </div>
                            <div class="col-md-6">&nbsp;</div>
                            <div class="col-md-12">
                                <div class="col-md-12" style="font-weight: 700;">
                                    <input type="checkbox" id="ckbCheckAll" /> Check All
                                </div>
                                <p class="title">&nbsp;</p>
                                <div class="col-md-2">
                                    <div class="user_access">
                                        <h4>Dashboard</h4>
                                        <div class="form-group">
                                            <label style="font-weight: 700;">
                                                <input type="checkbox" name="permission[]" value="admin" class="checkBoxClass flat-red" @if(in_array('admin',$permission)) checked @endif>
                                                       Dashboard
                                            </label>
                                        </div>
                                    </div> 
                                 </div>
                                 
                                <div class="col-md-2">
                                    <div class="user_access">
                                        <h4>Admin User</h4>
                                        <div class="form-group">
                                            <label style="font-weight: 700;">
                                                <input type="checkbox" name="permission[]" value="user-menu" class="checkBoxClass flat-red" @if(in_array('user-menu',$permission)) checked @endif >
                                                       &nbsp;User Main Menu
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="adduser" class="checkBoxClass flat-red" @if(in_array('adduser',$permission)) checked @endif>
                                                       &nbsp;Add User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="saveuser" class="checkBoxClass flat-red" @if(in_array('saveuser',$permission)) checked @endif>
                                                       &nbsp;Save User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="viewuser" class="checkBoxClass flat-red" @if(in_array('viewuser',$permission)) checked @endif>
                                                       &nbsp;View User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="edituser" class="checkBoxClass flat-red" @if(in_array('edituser',$permission)) checked @endif>
                                                       &nbsp;Edit User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="updateuser" class="checkBoxClass flat-red" @if(in_array('updateuser',$permission)) checked @endif>
                                                       &nbsp;Update User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="blockuser" class="checkBoxClass flat-red" @if(in_array('blockuser',$permission)) checked @endif>
                                                       &nbsp;Block User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="unblockuser" class="checkBoxClass flat-red" @if(in_array('unblockuser',$permission)) checked @endif>
                                                       &nbsp;Unblock User
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="addrole" class="checkBoxClass flat-red" @if(in_array('addrole',$permission)) checked @endif>
                                                       &nbsp;Add Role
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="saverole" class="checkBoxClass flat-red" @if(in_array('saverole',$permission)) checked @endif>
                                                       &nbsp; Save Role
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="viewrole" class="checkBoxClass flat-red" @if(in_array('viewrole',$permission)) checked @endif>
                                                       &nbsp; View Role
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="editrole" class="checkBoxClass flat-red" @if(in_array('editrole',$permission)) checked @endif>
                                                       &nbsp; Edit Role
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="updaterole" class="checkBoxClass flat-red" @if(in_array('updaterole',$permission)) checked @endif>
                                                       &nbsp; Update Role
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="roledelete" class="checkBoxClass flat-red" @if(in_array('roledelete',$permission)) checked @endif>
                                                       &nbsp;Delete Role
                                            </label>
                                        </div>
                                        <hr>
                                        <h4>Admin Profile</h4>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="admin-profile" class="checkBoxClass flat-red" @if(in_array('admin-profile',$permission)) checked @endif >
                                                       &nbsp;Profile
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="admin-change-password" class="checkBoxClass flat-red" @if(in_array('admin-change-password',$permission)) checked @endif >
                                                       &nbsp;Change Password
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-2">
                                    <div class="user_access">
                                        <h4>Product</h4>
                                        <div class="form-group">
                                            <label style="font-weight: 700;">
                                                <input type="checkbox" name="permission[]" value="product-menu" class="checkBoxClass flat-red" @if(in_array('product-menu',$permission)) checked @endif>
                                                      Product Main Menu
                                            </label>
                                        </div> 
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="addproduct" class="checkBoxClass flat-red" @if(in_array('addproduct',$permission)) checked @endif>
                                                       Add Product
                                            </label>
                                        </div> 
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="saveproduct" class="checkBoxClass flat-red" @if(in_array('saveproduct',$permission)) checked @endif>
                                                       Product Save
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="permission[]" value="productlist" class="checkBoxClass flat-red" @if(in_array('productlist',$permission)) checked @endif>
                                                       Product View
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                           </div>
                       </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $("#ckbCheckAll").click(function () {
            $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        });

        $(".checkBoxClass").change(function () {
            if (!$(this).prop("checked")) {
                $("#ckbCheckAll").prop("checked", false);
            }
        });
    });
</script>
@stop