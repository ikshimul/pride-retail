 @extends('admin.layouts.app')
@section('title', 'Add Admin User')
@section('content')

<section class="content-header">
      <h1>
        Manage User
        <small>Add</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Admin User</a></li>
        <li><a href="#">Manage User</a></li>
        <li class="active">Add User</li>
      </ol>
</section>
<section class="content">
 <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Add Admin User</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-2"></div>
                <form action="<?php echo url('admin/user/saveuser')?>" method="post" enctype="multipart/form-data" >
                    {{csrf_field()}}
                    <div class="col-md-5">
                        @if(session('save'))
                        <div class="alert alert-success" role="alert">
                            <?php echo session('save') ?>
                        </div>
                        @endif
                        @if(session('error'))
                        <div class="alert alert-success" role="alert">
                            <?php echo session('error') ?>
                        </div>
                        @endif
                        <div class="form-group">
                            <label>Full Name</label>
                            <input type="text" name="full_name" value="<?php echo old('full_name') ?>" class="form-control">
                            <span class="text-danger"><?php echo $errors->first('full_name') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" value="<?php echo old('email') ?>" class="form-control">
                            <span class="text-danger"><?php echo $errors->first('email') ?></span>
                        </div>
                        <div class="form-group">
                            <label>User Name</label>
                            <input type="text" name="username" value="<?php echo old('username')?>" class="form-control">
                            <span class="text-danger"><?php echo $errors->first('username') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="mobile" value="<?php echo old('mobile')?>" class="form-control">
                            <span class="text-danger"><?php echo $errors->first('mobile') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea type="text" name="address" class="form-control" rows="3"><?php echo old('address') ?></textarea>
                            <span class="text-danger"><?php echo $errors->first('address') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password"  class="form-control">
                            <span class="text-danger"><?php echo $errors->first('password') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" name="c_password" class="form-control">
                            <span class="text-danger"><?php echo $errors->first('c_password')?></span>
                        </div>
                        <div class="form-group">
                            <label>Role</label>
                            <select name="role_id" class="form-control">
                                <option value="">---- select role ----</option>
                                <?php foreach($role as $role_nma) { ?>
                                <option value="<?php echo $role_nma->id ?>"> <?php echo $role_nma->role_name ?></option>
                               <?php } ?>
                            </select>
                             <span class="text-danger"><?php echo $errors->first('role_id')?></span>
                        </div>
                        <div class="form-group">
                            <label>Profile Image</label>
                            <input id="input-upload-img1" type="file" class="file" name="photo" data-preview-file-type="text">
                            <span class="text-danger"><?php $errors->first('photo') ?></span>
                        </div>
                        <span class="text-danger"><?php $errors->first('photo') ?></span>
                    </div>
                    <div class="col-md-5"></div>
            </div>
        </div>
        <div class="box-footer">
            <input type="submit" name="btn" class="btn btn-success" value="Add User">
        </div>
        <form>
    </div>   
</section>
@stop