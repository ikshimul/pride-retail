@extends('admin.layouts.app')
@section('title', 'Admin User list')
@section('content')
<section class="content-header">
      <h1>
        Manage User
        <small>List</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Admin User</a></li>
        <li><a href="#">Manage User</a></li>
        <li class="active">View Users</li>
      </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            @if(session('save'))
            <div class="alert alert-success" role="alert">
                {{session('save')}}
            </div>
            @endif
            @if(session('error'))
            <div class="alert alert-danger" role="alert">
                {{session('error')}}
            </div>
            @endif
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">View all user with their responsibilities</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Full Name</th>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user_list)
                            <tr>
                                <td><img src="{{url('/')}}/storage/app/public/admin/user/{{$user_list->image}}" height="50" width="50"/></td>
                                <td>{{$user_list->name}}</td>
                                <td>{{$user_list->username}}</td>
                                <td>{{$user_list->email}}</td>
                                <td>{{$user_list->mobile}}</td>
                                <td>{{$user_list->address}}</td>
                                <td>{{$user_list->admin_type}}</td>
                                <td>
                                    <a href="{{url('admin/user/edituser/'.$user_list->id)}}" class="btn btn-sm btn-primary">Edit</a>
                                <?php if($user_list->admin_status==0){ ?>
                                    <a href="#" class="btn btn-sm btn-success" onclick="confirm_delete('{{url('admin/user/unblockuser/'.$user_list->id)}}');" title='Unblock admin'>Active</a>
                                 <?php } else{ ?>
                                   <a href="#" class="btn btn-sm btn-danger" onclick="confirm_delete('{{url('admin/user/blockuser/'.$user_list->id)}}');" title='Block admin'>Block</a>
                                 <?php } ?>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>	
<script>
    function confirm_delete(delete_url){
    jQuery("#modal-delete").modal('show', {backdrop:'static'});
    document.getElementById("delete_link").setAttribute('href', delete_url);
    }
</script>
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align:center;">Are you sure to block this admin?</h4>
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="#" class="btn btn-danger" id="delete_link">Block</a>
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
@stop  