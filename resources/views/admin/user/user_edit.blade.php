@extends('admin.layouts.app')
@section('title', 'Admin info Edit')
@section('content')
<section class="content-header">
      <h1>
        Manage User
        <small>Edit</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Admin User</a></li>
        <li><a href="#">Manage</a></li>
        <li class="active">Edit</li>
      </ol>
</section>
<section class="content">
    @if(session('save'))
    <div class="alert alert-success" role="alert">
        {{session('save')}}
    </div>
    @endif
    @if(session('error'))
    <div class="alert alert-success" role="alert">
        {{session('error')}}
    </div>
    @endif
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Update User</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-2"></div>
                <form action="{{url('admin/user/updateuser')}}" method="post" name="edit_user" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{$user->id}}">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Full Name</label>
                            <input type="text" name="name" value="{{$user->name}}" class="form-control">
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" value="{{$user->email}}" class="form-control">
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        </div>
                        <div class="form-group">
                            <label>User Name</label>
                            <input type="text" name="username" value="{{$user->username}}" class="form-control">
                            <span class="text-danger">{{ $errors->first('username') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea type="text" name="address" class="form-control" rows="3">{{$user->address}}</textarea>
                            <span class="text-danger">{{ $errors->first('address') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="mobile" value="{{$user->mobile}}" class="form-control">
                            <span class="text-danger"></span>
                        </div>
                     <!--   <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password"  class="form-control">
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" name="c_password" class="form-control">
                            <span class="text-danger">{{$errors->first('c_password')}}</span>
                        </div> --->
                        <div class="form-group">
                            <label>Role</label>
                            <select type="text" name="role_id" class="form-control">
                                <option value="">---- select role ----</option>
                                @foreach($role as $role_list)
                                <option value="{{$role_list->id}}">{{$role_list->role_name}}</option>
                                @endforeach
                            </select>
                            <span class="text-danger">{{$errors->first('role_id')}}</span>
                        </div>
                        <div class="form-group">
                            <label>Current Photo</label>
                            <div class="mb10">
                                <span class="file-input">
                                    <div class="file-preview">
                                        <div class="close fileinput-remove text-right"><a href="#" onclick="confirm_modal('#');">×</a></div>
                                        <div class="file-preview-thumbnails">
                                            <div class="file-preview-frame" id="preview">
                                                <img class="img-responsive" src="{{ URL::to('') }}/storage/app/public/admin/user/{{$user->image}}" class="file-preview-image" title="" alt="" style="width:auto;height:160px;">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>   
                                        <div class="file-preview-status text-center text-success"></div>
                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                    </div>

                                </span>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                            <label>Profile Image</label>
                            <input id="input-upload-img1" type="file" class="file" name="photo" data-preview-file-type="text"/>
                            <div class="help-block with-errors">{{ $errors->first('image') }}</div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="btn" class="btn btn-success" value="Update User">
        </div>
        <form>
    </div>
    <script>
        document.forms['edit_user'].elements['role_id'].value = '{{$user->role_id}}';
    </script>
    @stop