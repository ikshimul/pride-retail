<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
        <link rel="stylesheet" href="{{asset('assets_admin/bootstrap/css/bootstrap.min.css')}}">
        <link href="{{ asset('assets/css/register/opensans-font.css') }}" rel="stylesheet" />
        <link href="{{asset('assets/fonts/line-awesome/css/line-awesome.min.css')}}" rel="stylesheet" />
        <link href="{{ asset('assets/css/register/demo.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/register/sky-forms.css') }}?3" rel="stylesheet" />
        <link href="{{ asset('assets/css/register/sky-forms-purple.css') }}?1" rel="stylesheet" />
        <link rel="stylesheet" href="{{asset('assets/css/fileinput.min.css')}}">
        <style>
            .form-checkbox{
                display: inherit;
                height: auto;
                width: auto;
                border-width: 0px;
            }
            label{
                font-weight: 500;
            }
        </style>
        <script type="text/javascript">
          var base_url = "{{ URL::to('') }}";
          var csrf_token = "{{ csrf_token() }}";
        </script>
</head>
 <body class="font-sans antialiased">
    <div class="body">			
    <form method="POST" action="{{url('user/store')}}" id="register-form" class="register-form sky-form" enctype="multipart/form-data">
    {{ csrf_field() }}
    	<header>Registration form</header>
    	<fieldset>
    		<div class="row">
    			<div class="col col-6">
    				<section>
    					<label for="name" class="label">Full Name</label>
    					<label class="input">
    						<input type="text" name="name" id="name" value="{{ old('name') }}" required/>
    					</label>
    					<span class="help-block">
        					<strong>{{ $errors->first('name') }}</strong>
        				</span>
    				</section>
    			</div>
    			<div class="col col-6">
    			  <section>
    				<label class="label">Email</label>
    				<label class="input">
    					<input type="text" name="email" id="email" value="{{ old('email') }}"  pattern="[^@]+@[^@]+.[a-zA-Z]{2,6}"/>
    				</label>
    				<span class="help-block">
    					<strong>{{ $errors->first('email') }}</strong>
    				</span>
    			  </section>
    			</div>
    		</div>
    		<div class="row">
    		    <div class="col col-6">
    				<section>
    					<label for="nid_number" class="label">NID Number</label>
    					<label class="input">
    						<input type="text" name="nid_number" id="nid_number" value="{{ old('nid_number') }}" required pattern="^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$"/>
    					</label>
    					<span class="help-block">
        					<strong>{{ $errors->first('nid_number') }}</strong>
        				</span>
    				</section>
    			</div>
    			<div class="col col-6">
    				<section>
    					<label for="nid_pdf" class="label">NID PDF</label>
    					<label class="input">
    					    <input id="nid_pdf" type="file" class="file" name="nid_pdf" data-preview-file-type="text">
    					</label>
    					<span class="help-block">
        					<strong>{{ $errors->first('nid_pdf') }}</strong>
        				</span>
    				</section>
    			</div>
    		</div>
    		<div class="row">
    		    <div class="col col-6">
    				<section>
    					<label for="tarde_license" class="label">Trade License Number</label>
    					<label class="input">
    						<input type="text" name="tarde_license" id="tarde_license" value="{{ old('tarde_license') }}" required pattern="^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$"/>
    					</label>
    					<span class="help-block">
        					<strong>{{ $errors->first('tarde_license') }}</strong>
        				</span>
    				</section>
    			</div>
    			<div class="col col-6">
    				<section>
    					<label for="tarde_license_pdf" class="label">Trade License PDF</label>
    					<label class="input">
    					    <input id="tarde_license_pdf" type="file" class="file" name="tarde_license_pdf" data-preview-file-type="text">
    						<!--<input type="text" name="tarde_license_pdf" id="tarde_license_pdf" value="{{ old('tarde_license_pdf') }}" required/>-->
    					</label>
    					<span class="help-block">
        					<strong>{{ $errors->first('tarde_license_pdf') }}</strong>
        				</span>
    				</section>
    			</div>
    		</div>
    		<div class="row">
    		    <div class="col col-6">
    				<section>
    					<label for="phone" class="label">Phone Number</label>
    					<label class="input">
    						<input type="text" name="phone" id="phone" value="{{ old('phone') }}" required pattern="^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$"/>
    					</label>
    					<span class="help-block">
        					<strong>{{ $errors->first('phone') }}</strong>
        				</span>
    				</section>
    			</div>
    			<div class="col col-6">
    				<section>
    					<label for="country" class="label">Country</label>
    					<label class="input">
    						<input type="text" name="country" id="country" value="{{ old('country') }}"/>
    					</label>
    				</section>
    			</div>
    		</div>
    		<div class="row">
    		    <div class="col col-6">
    				<section>
    					<label for="region" class="label">Region</label>
    					<label class="select">
    					    <select class="select" name="region" id="region" required>
    					        <option value=''> -- Select Region -- </option>
    					        <?php foreach($regions as $region){ ?>
    					        <option value='{{$region->id}}'> {{$region->name}} </option>
    					        <?php } ?>
    					    </select>
    						<!--<input type="text" name="region" id="region" value="{{ old('region') }}" required/>-->
    					</label>
    				</section>
    			</div>
    			<div class="col col-6">
    				<section>
    					<label for="City" class="label">city</label>
    					<label class="select">
    					    <select class="select"  name="city" id="city" required>
    					        <option value=''> -- Select City -- </option>
    					    </select>
    					</label>
    					<!--<label class="input">-->
    						<!--<input type="text" name="city" id="city" value="{{ old('city') }}" required/>-->
    					<!--</label>-->
    				</section>
    			</div>
    		</div>
    		<section>
    			<label for="address" class="label">Address</label>
    			<label class="textarea textarea">
    				<textarea rows="3" name="address" id="address" required></textarea>
    			</label>
    			<div class="note"><strong>Note:</strong> e.g House No 54, Road 1, Sector 6, Uttara, Dhaka - 1230</div>
    		</section>
    		<div class="row">
    			<div class="col col-6">
    			  <section>
    				<label class="label" for="password">Password</label>
    				<label class="input">
    					<input type="password" name="password" id="password" class="input-text" required />
    				</label>
    				<span class="help-block">
    					<strong>{{ $errors->first('password') }}</strong>
    				</span>
    			  </section>
    			</div>
    			<div class="col col-6">
    			  <section>
    				<label for="comfirm-password" class="label">Confirm Password</label>
    				<label class="input">
    					<input type="password" name="comfirm_password" id="comfirm_password" class="input-text" required>
    				</label>
    				<span class="help-block">
    					<strong>{{ $errors->first('password_confirmation') }}</strong>
    				</span>
    			  </section>
    			</div>
    		</div>
    	</fieldset>
    	<fieldset>
    		<section>
    		<div class="form-checkbox">
    			<label class="container">
    				<p>I agree to the<a href="{{url('/privacy-cookies')}}" target="__blank"  class="text" style="color:#56b6e0;"> Privacy Policy</a>.</p>
    				<input type="checkbox" name="terms" id="terms" required />
    				<span class="checkmark"></span>
    			</label>
    			<span class="help-block">
    				<strong></strong>
    			</span>
    		</div>
    		<!--<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>I want to receive news and  special offers</label> -->
    		</section>
    	</fieldset>
    	<footer>
    		<button type="submit" class="button">Create an Account</button>
    	</footer>
    </form>		
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="{{asset('assets_admin/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/fileinput.min.js')}}"></script>
    <script src="{{ asset('assets/js/validation/jquery.validate.min.js') }}"></script>
    <script src="{{asset('assets/js/validation/additional-methods.min.js')}}"></script>
    <script>
    $( "#register-form" ).validate({
    	rules: {
    		password: "required",
    		comfirm_password: {
    			equalTo: "#password"
    		},
    		registeruser_dob : {
            required : false,
            date : true,
           }
    	},
    	messages: {
    		name: {
    			required: "Please enter your full name"
    		},
    // 		email: {
    // 			required: "Please provide an email"
    // 		},
    		nid_number: {
    			required: "Please provide your NID card number."
    		},
    		nid_pdf: {
    			required: "Please upload your NID pdf or image file."
    		},
    		tarde_license: {
    			required: "Please provide your Tarde License number."
    		},
    		tarde_license_pdf: {
    			required: "Please upload your Tarde License pdf or image file."
    		},
    		phone: {
    			required: "Please provide your phone number."
    		},
    		region: {
    			required: "Please select your region."
    		},
    		city: {
    			required: "Please select your city."
    		},
    		address: {
    			required: "Please enter your address."
    		},
            password: {
    	  		required: "Please enter your password"
    		},
    		comfirm_password: {
    			required: "Please confirm your password",
    			equalTo: "Password not match"
    		},		
            terms: {
    			required: "Please agree Privacy Policy"
    		}
    	}
    });
        $('#dob_cal').on('click', function() {
            $('#datepicker').datepicker('show');
        });
    </script>
    <script>
    $(document).ready(function($){
    	   $("#region").on('change',function(){
    	    var region_id =$("#region").val();
    		var url_op = base_url + "/ajax/get-citylist/" + region_id;
    		$.ajax({
    			url: url_op,
    			type: 'GET',
    			dataType: 'json',
    			data: '',
    			success: function (data) {
    				//alert(data);
    				$('#city').empty();
    				$('#city').append('<option value=""> select city </option>');
    				$.each(data, function (index, cityobj) {
    					$('#city').append('<option value="' + cityobj.id + '">' + cityobj.name + '</option>');
    				});
    				 $('#city').focus().select();
    			}
    		});
        });
    });
</script>
</body>
