@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="{{asset('assets/css/price_rangs.css')}}">
<section class="breadcrumb breadcrumb_bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="breadcrumb_iner">
                    <div class="breadcrumb_iner_item">
                        <p>Home / Login</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="login_part section_padding">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-6">
                <div class="login_part_text text-center">
                    <div class="login_part_text_iner">
                        <h2>New to our Shop?</h2>
                        <p>Create an account to take advantage of faster and easier shopping.</p>
                        <a href="#" class="btn_3">Create an Account</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="login_part_form">
                    <div class="login_part_form_iner">
                        <h3>Welcome Back ! <br>
                            Please Sign in now</h3>
                            @if (session('failed'))
                            <center>
                                <div class="mt-10 info text-danger">{{session('failed')}}
                                </div>
                            </center>
                            <br>
                            @endif 	
                            <form class="row contact_form" action="{{url('/login')}}" method="post" novalidate="novalidate">
                                {{ csrf_field() }}
                                <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control" id="mobile" name="mobile" value="{{ old('mobile') }}" placeholder="Mobile no.">
                                    @if ($errors->has('mobile'))
                                    <div class="mt-10 info text-danger">
                                        {{ $errors->first('mobile') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="password" class="form-control" id="password" name="password" value="" placeholder="Password">
                                    @if ($errors->has('password'))
                                    <div class="mt-10 info text-danger">
                                        {{ $errors->first('password') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="col-md-12 form-group">
                                    <div class="creat_account d-flex align-items-center">
                                        <input type="checkbox" id="f-option" name="selector">
                                        <label for="f-option">Remember me</label>
                                    </div>
                                    <button type="submit" value="submit" class="btn_3">
                                        log in
                                    </button>
                                    <a class="lost_pass" href="#">forget password?</a>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
