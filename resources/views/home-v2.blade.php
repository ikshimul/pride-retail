@extends('layouts.app-v2')

@section('content')
<section class="home-section home-full-height bg-dark bg-gradient" id="home" data-background="assets/images/section-10.jpg">
    <div class="titan-caption">
      <div class="caption-content">
        <div class="font-alt mb-30 titan-title-size-1">Hello &amp; welcome</div>
        <div class="font-alt mb-40 titan-title-size-4">We are Coming soon</div><a class="section-scroll btn btn-border-w btn-round" href="#">Shop Now</a>
      </div>
    </div>
</section>
@endsection