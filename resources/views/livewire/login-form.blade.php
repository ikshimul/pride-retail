<form class="row contact_form" wire:submit.prevent="submit" action="{{url('/login')}}" method="post" novalidate="novalidate">
    <div class="col-md-12 form-group p_star">
        <input type="text" class="form-control" id="mobile"  name="mobile" value="" placeholder="Mobile No.">
        @error('mobile') <span class="error">{{ $message }}</span> @enderror
    </div>
    <div class="col-md-12 form-group p_star">
        <input type="password" class="form-control" id="password"  name="password" value="" placeholder="Password">
        @error('password') <span class="error">{{ $message }}</span> @enderror
    </div>
    <div class="col-md-12 form-group">
        <div class="creat_account d-flex align-items-center">
            <input type="checkbox" id="f-option" name="selector">
            <label for="f-option">Remember me</label>
        </div>
        <button type="submit" value="submit" class="btn_3">
            log in
        </button>
        <a class="lost_pass" href="#">forget password?</a>
    </div>
</form>
                