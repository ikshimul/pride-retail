<nav class="navbar navbar-expand-lg navbar-light">
    <a class="navbar-brand" href="{{url('/')}}"> <img src="{{url('/')}}/storage/app/public/pride-logo.png" alt="logo"> </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="menu_icon"><i class="fas fa-bars"></i></span>
    </button>
    <div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="{{url('/')}}">Home</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown_1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Women
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown_1">
                    <a class="dropdown-item" href="#"> shop category</a>
                </div>
            </li>
            <!--<li class="nav-item dropdown">-->
            <!--    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown_3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
            <!--        pages-->
            <!--    </a>-->
            <!--    <div class="dropdown-menu" aria-labelledby="navbarDropdown_2">-->
            <!--        <a class="dropdown-item" href="{{url('/login')}}">-->
            <!--            login-->
            <!--        </a>-->
            <!--        <a class="dropdown-item" href="#">tracking</a>-->
            <!--        <a class="dropdown-item" href="#">product checkout</a>-->
            <!--        <a class="dropdown-item" href="#">shopping cart</a>-->
            <!--        <a class="dropdown-item" href="#">confirmation</a>-->
            <!--        <a class="dropdown-item" href="#">elements</a>-->
            <!--    </div>-->
            <!--</li>-->
            <!--<li class="nav-item">-->
            <!--    <a class="nav-link" href="#">Contact</a>-->
            <!--</li>-->
        </ul>
    </div>
    <div class="hearer_icon d-flex">
        <div class="nav-item dropdown">
             @guest
            <a class="dropdown-toggle" href="#" id="navbarDropdown_3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ti-user"></i>
            </a>
            @else
            <a class="dropdown-toggle" href="#" id="navbarDropdown_3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->name }}
            </a>
            @endguest
            @guest
              <div class="dropdown-menu" aria-labelledby="navbarDropdown_3">
                    <a class="dropdown-item" href="{{ url('/login') }}">Login</a>
            </div>
             @else
           <div class="dropdown-menu" aria-labelledby="navbarDropdown_3">
                    <a class="dropdown-item" href="{{ route('dashboard') }}"> My Account</a>
                    <a class="dropdown-item" href="{{ url('/logout') }}">Logout</a>
            </div>
            @endguest
        </div>
        <div class="dropdown cart">
            <a class="dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ti-bag"></i>
            </a>
           
        </div>
        <a id="search_1" href="javascript:void(0)"><i class="ti-search"></i></a>
    </div>
</nav>
