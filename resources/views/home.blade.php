@extends('layouts.app')

@section('content')

<section class="banner_part">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <div class="banner_slider">
                    <div class="single_banner_slider">
                        <div class="banner_text">
                            <div class="banner_text_iner">
                                <h5 style="color:white;">New Collection</h5>
                                <h1 style="color:white;">Collection 2020</h1>
                                <a href="#" class="btn_1">shop now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="feature_part pt-4">
    <div class="container-fluid p-lg-0 overflow-hidden">
        <div class="row align-items-center justify-content-between">
            <div class="col-lg-3 col-sm-6">
                <div class="single_feature_post_text">
                    <img src="{{url('/')}}/storage/app/public/banner/1.jpg" alt="#">
                    <div class="hover_text">
                        <!--<a href="#" class="btn_2">shop for male</a>-->
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single_feature_post_text">
                    <img src="{{url('/')}}/storage/app/public/banner/4.jpg" alt="#">
                    <div class="hover_text">
                        <!--<a href="#" class="btn_2">shop for male</a>-->
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single_feature_post_text">
                    <img src="{{url('/')}}/storage/app/public/banner/6.jpg" alt="#">
                    <div class="hover_text">
                        <!--<a href="#" class="btn_2">shop for male</a>-->
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single_feature_post_text">
                    <img src="{{url('/')}}/storage/app/public/banner/7.jpg" alt="#">
                    <div class="hover_text">
                        <!--<a href="#" class="btn_2">shop for male</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</br>
<section class="shipping_details section_padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="single_shopping_details">
                    <img src="{{url('/')}}/storage/app/public/icon/icon_1.png" alt="">
                    <h4>Free shipping</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single_shopping_details">
                    <img src="{{url('/')}}/storage/app/public/icon/icon_2.png" alt="">
                    <h4>Shipping Policy</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single_shopping_details">
                    <img src="{{url('/')}}/storage/app/public/icon/icon_3.png" alt="">
                    <h4>Payment Method</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single_shopping_details">
                    <img src="{{url('/')}}/storage/app/public/icon/icon_4.png" alt="">
                    <h4>Free shipping</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
