<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="Pride, Pride Retail,pride wholesale" />
        <meta name="keywords" content="Pride, Pride group, Pride retail, pride wholesale" />
        <title>{{ config('app.name', 'Laravel') }}</title>
        <link rel="icon" href="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <!--<meta name="robots" content="INDEX,FOLLOW"/>-->
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="author" content="Pride Group">
         
        <!--stylesheet-->
        @livewire('asset.css')
       @livewireStyles
    </head>
    <body>
        <header class="main_menu home_menu">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-center">
                    <div class="col-lg-11">
                        @livewire('nav')
                    </div>
                </div>
            </div>
            <div class="search_input" id="search_input_box">
                <div class="container ">
                    <form class="d-flex justify-content-between search-inner">
                        <input type="text" class="form-control" id="search_input" placeholder="Search Here">
                        <button type="submit" class="btn"></button>
                        <span class="ti-close" id="close_search" title="Close Search"></span>
                    </form>
                </div>
            </div>
        </header>
        <!-- main content --->
            @yield('content')
        <!-- end main content ---->
        @livewire('footer')
    @livewire('asset.js')
    @livewireScripts
    </body>
</html>
