<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="Pride, Pride Retail,pride wholesale" />
        <meta name="keywords" content="Pride, Pride group, Pride retail, pride wholesale" />
        <title>{{ config('app.name', 'Laravel') }}</title>
        <link rel="icon" href="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <!--<meta name="robots" content="INDEX,FOLLOW"/>-->
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="author" content="Pride Group">
        <!--  
        Favicons
        =============================================
        -->
        <link rel="apple-touch-icon" sizes="57x57" href="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <link rel="icon" type="image/png" sizes="192x192" href="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ URL::to('') }}/storage/app/public/logo_fav.png">
        <meta name="theme-color" content="#ffffff">
        <!--  
        Stylesheets
        =============================================
        
        -->
        <!-- Default stylesheets-->
        <link rel="stylesheet" href="{{asset('assets/v2/lib/bootstrap/dist/css/bootstrap.min.css')}}">
        <!-- Template specific stylesheets-->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('assets/v2/lib/animate.css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('assets/v2/lib/components-font-awesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/v2/lib/et-line-font/et-line-font.css')}}">
        <link rel="stylesheet" href="{{asset('assets/v2/lib/et-line-font/et-line-font.css')}}">
        <link rel="stylesheet" href="{{asset('assets/v2/lib/flexslider/flexslider.css')}}">
        <link rel="stylesheet" href="{{asset('assets/v2/lib/owl.carousel/dist/assets/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/v2/lib/owl.carousel/dist/assets/owl.theme.default.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/v2/lib/magnific-popup/dist/magnific-popup.css')}}">
        <link rel="stylesheet" href="{{asset('assets/v2/lib/simple-text-rotator/simpletextrotator.css')}}">
        <!-- Main stylesheet and color file-->
        <link rel="stylesheet" href="{{asset('assets/v2/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('assets/v2/css/default.css')}}">
        <style>
            .logo{
                position: relative;
                top: -30px;
                left: 0px;
                width: 80%;
                  }
            @media only screen and (max-width: 600px) {
              .logo{
                    position: relative;
                    top: -24px;
                    left: 0;
                    width: 82px;
              }
            }
        </style>
    </head>
    <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
        <main>
          <div class="page-loader">
            <div class="loader">Loading...</div>
          </div>
          <nav class="navbar navbar-custom navbar-transparent navbar-fixed-top one-page" role="navigation">
            <div class="container">
              <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                <a class="navbar-brand" href="{{url('/')}}"><img class="logo" src="{{ URL::to('') }}/storage/app/public/logo_fav.png"/></a>
              </div>
              <div class="collapse navbar-collapse" id="custom-collapse">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="{{url('/products')}}">Products</a></li>
                  <li><a href="#">Login</a></li>
                </ul>
              </div>
            </div>
          </nav>
          <!-- main content --->
            @yield('content')
          <!-- end main content ---->
          <div class="main">
            <hr class="divider-d">
            <footer class="footer bg-dark">
              <div class="container">
                <div class="row">
                  <div class="col-sm-6">
                    <p class="copyright font-alt">&copy; 2020&nbsp;<a href="#">Pride Group</a>, All Rights Reserved</p>
                  </div>
                  <div class="col-sm-6">
                    <div class="footer-social-links"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-dribbble"></i></a><a href="#"><i class="fa fa-skype"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </footer>
          </div>
          <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
          <!--<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>-->
        </main>
       <!--  
        JavaScripts
        =============================================
        -->
        <script src="{{asset('assets/v2/lib/jquery/dist/jquery.js')}}"></script>
        <script src="{{asset('assets/v2/lib/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/v2/lib/wow/dist/wow.js')}}"></script>
        <script src="{{asset('assets/v2/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js')}}"></script>
        <script src="{{asset('assets/v2/lib/isotope/dist/isotope.pkgd.js')}}"></script>
        <script src="{{asset('assets/v2/lib/imagesloaded/imagesloaded.pkgd.js')}}"></script>
        <script src="{{asset('assets/v2/lib/flexslider/jquery.flexslider.js')}}"></script>
        <script src="{{asset('assets/v2/lib/owl.carousel/dist/owl.carousel.min.js')}}"></script>
        <!--<script src="{{asset('assets/v2/lib/smoothscroll.js')}}"></script>-->
        <script src="{{asset('assets/v2/lib/magnific-popup/dist/jquery.magnific-popup.js')}}"></script>
        <script src="{{asset('assets/v2/lib/simple-text-rotator/jquery.simple-text-rotator.min.js')}}"></script>
        <!--<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>-->
        <script src="{{asset('assets/v2/js/plugins.js')}}"></script>
        <script src="{{asset('assets/v2/js/main.js')}}"></script>
    </body>
</html>