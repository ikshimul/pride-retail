<?php

namespace App\Http\Controllers\Auth;

use Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\login as RequestLogin;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    public function login(RequestLogin $request) {
        $vaildation = Validator::make($request->all(), [
                    'mobile' => 'required',
                    'password' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $user_login = ['username' => $request->mobile, 'password' => $request->password];
            if (Auth::attempt($user_login, true)) {
                return redirect()->intended('/checkout');
            } else {
				return redirect()->back()->with('failed', 'Your mobile no or password is not correct or is not registered at retail.pride-limited.com')->withInput();
                //return redirect()->back()->with('failed', 'Your e-mail address or password is not correct or is not registered at urban-truth.com.')->withInput();
            }
        }
    }
    
    public function logout(Request $request) {
      Auth::logout();
      return redirect('/login');
    }
}
