<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\Models\Admin;
use DB;
use Carbon\Carbon;
use Illuminate\Support\MessageBag;

class AdminLoginController extends Controller
{
	public function __construct(){
		$this->middleware('guest:admin',['except'=>['logout']]);
	}
	public function ShowLoginForm(){
		return view('auth.admin.login');
	}
	
	public function Login(Request $request){
	   
		$this->validate($request,[
		  'username'=>'required',
		  'password'=>'required'
		]);
		 
		if(Auth::guard('admin')->attempt(['username'=>$request->username,'password'=>$request->password, 'admin_status'=>1],$request->remember)){
		    $update['admin_ip'] = request()->ip();
            $update['admin_lastlogin'] = Carbon::now()->toDateTimeString();
		    Admin::where('id',Auth::guard('admin')->user()->id)->update($update);
			return redirect()->intended(route('admin.dashboard'));
		}else{
			$errors=new MessageBag(['username'=>['These credentials do not match our records.']]);
			return redirect()->back()->withErrors($errors)->withInput($request->only('username','remember'));
		}
	}
	
	public function logout(){
		Auth::guard('admin')->logout();
		return redirect('/admin');
	}
}
