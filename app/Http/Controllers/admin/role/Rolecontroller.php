<?php

namespace App\Http\Controllers\admin\role;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Rolepermission;

class Rolecontroller extends Controller
{
    public function __construct(Role $role, Rolepermission $role_permission) {
       $this->middleware('AdminAuth');
       $this->middleware('permission');
       $this->Role = $role;
       $this->Rolepermission = $role_permission;
    }
    
    public function index() {
        return view('admin.role.role_form');
    }
    
    public function save(Request $request) {
        $this->validate($request, [
            'role_name' => 'required|unique:roles,role_name'
        ]);
        $data = [
            'role_name' => $request->role_name
        ];
        $role=new Role();
        $role->role_name=$request->role_name;
        $role->save();
        $id=$role->id;
        if ($id) {
            $permission = $request->permission;
            foreach ($permission as $pinfo) {
                $role_permission = new Rolepermission();
                $role_permission->role_id = $id;
                $role_permission->permission = $pinfo;
                $role_permission->save();
            }
            $request->session()->flash('save', 'New role add successfully');
        } else {
            $request->session()->flash('error', 'New role not added');
        }
        return redirect()->back();
    }
    
    public function view() {
        $data['role'] = Role::where('active',0)->get();
        return view('admin.role.view_role')->with($data);
    }
    
    public function edit(Request $request) {
        $role_id = $request->id;
        $data['role'] = Role::find($role_id);
        $data['permission'] = $this->Rolepermission->get_permission($role_id);
        return view('admin.role.edit_role')->with($data);
    }
    
    public function update(Request $request) {
        $role_id = $request->id;
        //dd($role_id);
        Role::where('id', $role_id)->update(['role_name' => $request->role_name]);
        Rolepermission::where('role_id', $role_id)->delete();
        $permission = $request->permission;
        foreach ($permission as $p_info) {
            Rolepermission::create([
                'role_id' => $role_id,
                'permission' => $p_info
            ]);
        }
         $request->session()->flash('update', 'Role  Eidted Successfully!.');
         return redirect()->back();
    }
    
    public function delete($id){
        Role::where('id', $id)->update(['active' => 1]);
        return redirect()->back()->with('error_message', 'Role deleted');
    }
    
}
