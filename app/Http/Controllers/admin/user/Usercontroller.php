<?php

namespace App\Http\Controllers\admin\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Role;
use App\Models\Rolepermission;
use App\Models\Admin;
use Image;
use Validator;
use Storage;
use Auth;

class Usercontroller extends Controller
{
    protected $redirectTo = 'admin/login';

    public function __construct(Rolepermission $role_permission) {
         $this->middleware('AdminAuth');
         $this->middleware('permission');
         $this->Rolepermission = $role_permission;
    }
    
    public function index() {
        $data['role'] = Role::where('active',0)->get();
        return view('admin.user.add_user', $data);
    }
    
    public function save(Request $request) {
        $vaildation = Validator::make($request->all(), [
                    'full_name' => 'required',
                    'email' => 'required|email|unique:admins,email',
                    'username' => 'required|max:10|unique:admins,username',
                    'password' => 'required|min:6',
                    'c_password' => 'required|same:password',
                    'role_id' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $file = $request->file('photo');
            $savename = time() . '_' . $file->getClientOriginalName();
            Storage::put('public/admin/user/' . $savename, file_get_contents($request->file('photo')->getRealPath()));
            $role_info=Role::where('id',$request->role_id)->first();
            $user= Admin::create([
                'username' => $request->username,
                'name' => $request->full_name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'image' => $savename,
                'mobile' => $request->mobile,
                'address' => $request->address,
                'role_id' => $request->role_id,
                'admin_status' => 1,
                'admin_type' => $role_info->role_name
            ]);
            if($user){
            return redirect()->back()->with('save', 'New admin user create successfully.');
            }else{
                return redirect()->back()->with('error', 'admin user create failed.');
            }
        }
    }
    
    public function view() {
        $data['users'] = Admin::get();
        return view('admin.user.view_user', $data);
    }
    
    public function edit($id) {
        $data['user'] = Admin::where('id', $id)->first();
        $data['role'] = Role::where('active',0)->get();
        return view('admin.user.user_edit')->with($data);
    }
    
    public function update(Request $request) {
       $rules = [
            'name' => 'required',
            'username' => Rule::unique('admins','username')->ignore($request->id, 'id'),
            'email' => Rule::unique('admins','email')->ignore($request->id, 'id'),
        ];
        $vaildation = Validator::make($request->all(), $rules);
        //dd($vaildation);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $admin=Admin::find($request->id);
            if ($request->hasFile('photo')) {
                $file = $request->file('photo');
                $savename = time() . '_' . $file->getClientOriginalName();
                Storage::put('public/admin/user/' . $savename, file_get_contents($request->file('photo')->getRealPath()));
                $admin->image=$savename;
            }
            if ($request->has('role_id')) {
               $role_info=Role::where('id',$request->role_id)->first(); 
               $admin->role_id=$request->role_id;
               $admin->admin_type = $role_info->role_name;
            }
            $admin->name=$request->name;
            $admin->email=$request->email;
            $admin->username=$request->username;
            $admin->mobile=$request->mobile;
            $admin->address=$request->address;
            $admin->updated_by=Auth::guard('admin')->user()->id;
            $update=$admin->save();
            //dd($data);
            //$update=Admin::where('id',$request->id)->update($data);
            if($update){
            return redirect()->back()->with('save', 'Admin user update successfully.');
            }else{
                return redirect()->back()->with('error', 'admin user update failed.');
            }
        }
    }
    
    public function block($admin_id) {
        $admin=Admin::find($admin_id);
        $admin->updated_by=Auth::guard('admin')->user()->id;
        $admin->admin_status=0;
        $result =$admin->save();
        if ($result) {
            return redirect()->back()->with('save', 'Admin block successfully');
        } else {
            return redirect()->back()->with('error', 'Admin not block');
        }
    }
    
    public function Unblock($admin_id){
        $admin=Admin::find($admin_id);
        $admin->updated_by=Auth::guard('admin')->user()->id;
        $admin->admin_status=1;
        $result =$admin->save();
        if ($result) {
            return redirect()->back()->with('save', 'Admin Active successfully');
        } else {
            return redirect()->back()->with('error', 'Admin not active.');
        }
    }
    
}
