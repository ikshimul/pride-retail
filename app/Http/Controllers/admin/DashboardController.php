<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Rolepermission;

class DashboardController extends Controller
{
    public function __construct() {
        $this->middleware('AdminAuth');
    }
    
    public static function getRolePermission() {
        $role_id = Auth::guard('admin')->user()->role_id;
        $role_permission = Rolepermission::where('role_id', $role_id)->get();
        $permission = [];
        foreach ($role_permission as $p_list) {
            $permission[] = $p_list->permission;
        }
        return $permission;
    }
    
    public function index()
    {
        if(Auth::guard('admin')->user()->role_id == 1 || Auth::guard('admin')->user()->role_id == 2){
            return view('admin.dashboard');
        }elseif(Auth::guard('admin')->user()->role_id == 3){
            echo 'Delivery Dashboard';
        }
    }
}
