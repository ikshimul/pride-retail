<?php

namespace App\Http\Controllers\admin\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Productsize;
use DB;

class Managestock extends Controller
{
    public function __construct() {
        $this->middleware('AdminAuth');
    }
    
    public static function GetSizeQty($id){
        $productsize=new Productsize();
        return  $productsize->productsizes($id);
    }
    
    public static function GetExpectSize($product_id,$color_name){
        $prosizes = DB::select("SELECT *  FROM prosizes where sizename not in(select productsize_size  FROM productsizes where product_id='$product_id' and color_name='$color_name')");
        return $prosizes;
    }
}
