<?php

namespace App\Http\Controllers\admin\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Productsize;

class Managecolor extends Controller
{
    public function __construct() {
        $this->middleware('AdminAuth');
    }
    
    public static function GetColorProSize($id){
        $productsize=new Productsize();
        return  $productsize->productcolors($id);
    }
}
