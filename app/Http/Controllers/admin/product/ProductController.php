<?php

namespace App\Http\Controllers\admin\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;
use Validator;
use Image;
use Session;
use Carbon\Carbon;
use Auth;
use App\Models\Procat;
use App\Models\Subprocat;
use App\Models\Prosize;
use App\Models\Product;
use App\Models\Productsize;
use App\Models\Productprice;
use App\Models\Productalbum;
use App\Models\Productimg;

class ProductController extends Controller
{
    public function __construct() {
        $this->middleware('AdminAuth');
    }
    
    public function create() {
        $value = Auth::guard('admin')->user()->id;
        $data['total_product'] = Product::count();
        $data['procat_list'] = Procat::all();
        $data['avail_size'] = Prosize::all();
        return view('admin.product.add', $data);
    }
    
    public function store(Request $request){
        //dd($request);
        $created_by=Auth::guard('admin')->user()->id;
        $totalnumberofinserteddata = 0;
        $numberofboxedchecked = 0;
        $current_date = date("d-m-Y");
        $vaildation = Validator::make($request->all(), [
                    'txtproductname' => 'required|min:4',
                    'txtprice' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            if ($request->campaign) {
                $specialcollection = $request->campaign;
            } else {
                $specialcollection = 0;
            }
            
            $product=new Product();
            $product->procat_id=$request->ddlprocat;
            $product->subprocat_id=$request->ddlprosubcat1;
            $product->product_name=$request->txtproductname;
            $product->fabric=$request->fabric;
            $product->product_styleref=$request->txtstyleref;
            $product->product_description=$request->txtproductdetails;
            $product->product_care=$request->txtproductcare;
            $product->created_by=$created_by;
            $product->isSpecial=$specialcollection;
            $product->save();
            $product_id=$product->id;
            
            $productprice=new Productprice();
            $productprice->product_id=$product_id;
            $productprice->retail_rate=$request->txtprice;
            $productprice->hole_sale_rate=$request->txtprice;
            $productprice->distrubutor_rate=$request->txtprice;
            $productprice->distrubutor_rate=$request->txtprice;
            $productprice->effective_date='2020-12-31';
            $productprice->save();
            
            $total_grp = $request->total_grp;
            for ($loopcountermain = 1; $loopcountermain <= $total_grp; $loopcountermain++) {
                $txtcolorvalname = "txtcolorname_" . $loopcountermain;
                $color_name_post = "txtcolorname_" . $loopcountermain;
                $txtcolovalue = $request->$color_name_post;
                $colorfilethm = "file_colorthm_" . $loopcountermain;
                $totalnumberofavailabesize = session('numberofavailabesize');
                for ($loopcountersize = 1; $loopcountersize <= $totalnumberofavailabesize; $loopcountersize++) {
                    $checkboxvarname = "size" . $loopcountersize . "_" . $loopcountermain;
                    $checkboxvarvalename = "input_size" . $loopcountersize . "_" . $loopcountermain;
                    $checkboxvarvalename_barcode = "barcode_input_size" . $loopcountersize . "_" . $loopcountermain;
                    if ($request->$checkboxvarname == TRUE) {
                        $productsizaname = $request->$checkboxvarname;
                        $productsizquantity = $request->$checkboxvarvalename;
                        $barcode=$request->$checkboxvarvalename_barcode;
                        $numberofboxedchecked++;
                        if ($productsizquantity > 0) {
                            
                        } else {
                            $productsizquantity = 0;
                        }
                        $productsize=new Productsize();
                        $productsize->product_id = $product->id;
                        $productsize->barcode = $barcode;
                        $productsize->productsize_size = $productsizaname;
                        $productsize->SizeWiseQty = $productsizquantity;
                        $productsize->color_name = $txtcolovalue;
                        $productsize->save();
                        $productsize_id=$productsize->id;
                        if ($productsize_id) {
                            $totalnumberofinserteddata++;
                        } else {
                            
                        }
                    } else {
                        
                    }
                }
                if ($totalnumberofinserteddata == $numberofboxedchecked) {
                    //dd($request);
                    $productalbum=new Productalbum();
                    $productalbum->product_id=$product->id;
                    $productalbum->productalbum_name=$txtcolovalue;
                    $productalbum->productalbum_order=$loopcountermain;
                    $productalbum->save();
                    $productalbum_id=$productalbum->id;
                    if ($productalbum_id) {
                        if ($request->hasFile($colorfilethm)) {
                            $file = $request->file($colorfilethm);
                            $savename = $productalbum_id . '_album_thm_' . $file->getClientOriginalName();
                            $image = Image::make($request->file($colorfilethm))->save('storage/app/public/product/album/' . $savename);
                            if ($image) {
                                Productalbum::where('id', $productalbum_id)->update(['productalbum_img'=>$savename]);
                                for ($filecounter = 1; $filecounter <= 6; $filecounter++) {
                                    $imageid = NULL;
                                    $imgfieldname = "file_im" . $filecounter . "_" . $loopcountermain;
                                    if ($request->hasFile($imgfieldname)) {
                                        $file = $request->file($imgfieldname);
                                        $filename = $product_id . "_product_image_" . $filecounter . "_" . $file->getClientOriginalName();
                                        $image = Image::make($request->file($imgfieldname))->save('storage/app/public/product/image/' . $filename);
                                        $productimg=new Productimg();
                                        $productimg->productalbum_id=$productalbum_id;
                                        $productimg->productimg_order='';
                                        $productimg->save();
                                        $productimg_id = $productimg->id;
                                        if ($image) {
                                            $width = Image::make($request->file($imgfieldname))->width();
                                            $height = Image::make($request->file($imgfieldname))->height();
                                            if ($width >= 1590 && $width <= 1610 && $height >= 2390 && $height <= 2410) {
                                                //upload thm file
                                                $filenamethmb = $product_id . "_product_image_" . $filecounter . "_thm_" . $file->getClientOriginalName();
                                                $image = Image::make($request->file($imgfieldname))->resize(200, 300)->save('storage/app/public/product/image/' . $filenamethmb);
                                                //upload tiny file
                                                $filenametiny = $product_id . "_product_image_" . $filecounter . "_tiny_" . $file->getClientOriginalName();
                                                $image = Image::make($request->file($imgfieldname))->resize(64, 96)->save('storage/app/public/product/image/' . $filenametiny);
                                                //upload medium file
                                                $filenamemedium = $product_id . "_product_image_" . $filecounter . "_medium_" . $file->getClientOriginalName();
                                                $image = Image::make($request->file($imgfieldname))->resize(370, 555)->save('storage/app/public/product/image/' . $filenamemedium);
                                                Productimg::where('id', $productimg_id)->update(['productimg_img'=>$filename,'productimg_img_tiny'=>$filenametiny,'productimg_img_medium'=>$filenamemedium,'productimg_img_thm'=>$filenamethmb,'productimg_order'=>$filecounter]);
                                                if ($filecounter == 1 && $loopcountermain == 1) {
                                                    Product::where('id',$product->id)->update(['product_img_thm'=>$filenamemedium]);
                                                } else {
                                                    
                                                }
                                            } else {
                                                $productimg = Productimg::find($productimg_id);
                                                $productimg->delete();
                                                
                                                $productalbum = Productalbum::find($productalbum_id);
                                                $productalbum->delete();

                                                Product::where('id',$product_id)->update(['product_active_deactive'=>1,'trash'=>1]);
                                                return redirect()->back()->with('error', 'Image size problem.Upload image size must be equal 1600 X 2400');
                                            }
                                        }
                                    }
                                }
                            } else {
                                
                            }
                        } else {
                            
                        }
                    } else {
                        return redirect()->back()->with('error', 'Product ablum not inserted!');
                    }
                } else {
                    
                }
            }
            return redirect()->back()->with('save', 'Product upload successfully!');
        }
    }
    
    public function list(Request $request){
        $product=new Product();
        $subpro_id =$request->scid;
        $data['sub_pro_selected'] = $subpro_id;
        $subpro = new Subprocat();
        $data['product_list'] = $product->adminlist($subpro_id);
        $data['sub_category'] = $subpro->getall();
        $data['subpro_id'] = $subpro_id;
        return view('admin.product.list', $data);
    }
}
