<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\City;
use DB;

class AjaxController extends Controller
{
    public function getregionwisecity($region_id){
        $cities=City::where('region_id',$region_id)->get();
        return json_encode($cities);
    }
    
    public function GetProWiseSubpro($id) {
        $subpro = DB::table('subprocats')
                ->join('procats', 'procats.id', '=', 'subprocats.procat_id')
                ->select('subprocats.id', 'subprocats.subprocat_name', 'subprocats.subprocat_order', 'subprocats.subprocat_img','subprocats.procat_id', 'procats.procat_name')
                ->where('subprocats.procat_id', $id)
                ->get();
        return json_encode($subpro);
    }
}
