<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Region;
use App\Models\City;

class LocationController extends Controller
{
    public function getregion(){
        $list= Region::all();
        return response(['regions' => $list], 200);
    }
    
    public function getcity($id){
        $list= City::where('region_id',$id)->get();
        return response(['cities' => $list], 200);
    }
    
}
