<?php

namespace App\Http\Controllers\api\customer;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Userdetail;
use App\Models\Region;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Storage;
use Auth;
use App\Notifications\Signup;

class RegisterController extends Controller
{
    public function store(Request $request) {
        $messages = [
            'name.required' => 'Please enter your full name.',
            'nid_number.required' => 'Please enter your NID number.',
            'nid_pdf.required' => 'Please upload your NID pdf or image file.',
            'tarde_license.required' => 'Please enter your Tarde License number.',
            'tarde_license_pdf.required' => 'Please upload your Tarde License pdf or image file.',
            'password.required' => 'The Password field is required.',
            'comfirm_password.required' => 'The Conforim Password field is required.',
        ];
        $vaildation = Validator::make($request->all(), [
                    'name' => 'required',
                    'nid_number' => 'required|unique:userdetails,nid_number',
                    'nid_pdf' => 'required',
                    'tarde_license' => 'required|unique:userdetails,tarde_license',
                    'tarde_license_pdf' => 'required',
                    'phone' => 'required|numeric|min:11',
                    'region' => 'required|numeric',
                    'city' => 'required|numeric',
                    'address' => 'required',
                    'password' => 'required|min:4',
                    'comfirm_password' => 'required|same:password'
                        ], $messages);
        if ($vaildation->fails()) {
           return response()->json($vaildation->errors(),400);
        } else {
            $user = User::create([
            'name' => $request->name,
            'username' => $request->phone,
            'email' => $request->email,
            'password' => Hash::make($request->password),
           ]);
           $details=new Userdetail();
           $details->user_id=$user->id;
           $details->tarde_license=$request->tarde_license;
           if ($request->hasFile('tarde_license_pdf')) {
               $file = $request->file('tarde_license_pdf');
               $savename = time() . '_' . $file->getClientOriginalName();
               Storage::put('public/customer/tarde_license/' . $savename, file_get_contents($request->file('tarde_license_pdf')->getRealPath()));
               $details->tarde_license_pdf=$savename;
           }
           $details->nid_number=$request->nid_number;
           if ($request->hasFile('nid_pdf')) {
               $file = $request->file('nid_pdf');
               $savename = time() . '_' . $file->getClientOriginalName();
               Storage::put('public/customer/nid/' . $savename, file_get_contents($request->file('nid_pdf')->getRealPath()));
               $details->nid_pdf=$savename;
           }
           $details->phone=$request->phone;
           $details->address=$request->address;
           $details->country=$request->country;
           $details->region=$request->region;
           $details->city=$request->city;
           $details->save();
           if ($request->has('email')) {
              $user->notify(new Signup($request));
           }
           
           $response = [
                'result' => 'Success'
            ];
        
             return response($response, 200);
        }
        
    }
}
