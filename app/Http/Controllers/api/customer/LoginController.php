<?php

namespace App\Http\Controllers\api\customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Userdetail;

class LoginController extends Controller
{
    public function login(Request $request) {
        $vaildation = Validator::make($request->all(), [
                    'mobile' => 'required',
                    'password' => 'required'
        ]);
        if ($vaildation->fails()) {
            return response()->json($vaildation->errors(),400);
        } else {
            $user_login = ['username' => $request->mobile, 'password' => $request->password];
            if (Auth::attempt($user_login, true)) {
                $id=Auth::user()->id;
                $userinfo=Userdetail::where('user_id',$id)->first();
                $response = ['userinfo' => $userinfo];
                return response()->json($response,200);
            }else{
                $response = ['failed' => 'Your mobile no or password is not correct or is not registered'];
                return response()->json($response,400);
            }
        }
    }
}
