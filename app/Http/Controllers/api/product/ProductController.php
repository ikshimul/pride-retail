<?php

namespace App\Http\Controllers\api\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    public function productinfo(){
        $list= Product::where('product_active_deactive',0)->select('product_styleref','product_name','fabric','product_description','product_img_thm','product_care')->get();
        return response(['products' => $list], 200);
    }
}
