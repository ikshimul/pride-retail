<?php

namespace App\Http\Controllers\api\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Productalbum;
use App\Models\Productimg;

class ProductImage extends Controller
{
    public function album(){
        $list= Productalbum::all();
        return response(['albums' => $list], 200);
    }
    
     public function images(){
        $list= Productimg::all();
        return response(['images' => $list], 200);
    }
}
