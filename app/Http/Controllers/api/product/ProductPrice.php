<?php

namespace App\Http\Controllers\api\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Productprice;

class ProductPrice extends Controller
{
    public function index(){
        $list= Productprice::all();
        return response(['prices' => $list], 200);
        
    }
}
