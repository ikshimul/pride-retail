<?php

namespace App\Http\Controllers\api\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Productsize;

class ProductStock extends Controller
{
     public function index(){
        $list= Productsize::all();
        return response(['stocks' => $list], 200);
        
    }
}
