<?php

namespace App\Http\Controllers\api\agent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;

class AgentController extends Controller
{
    public function list(){
        $list= Admin::where('admin_type','Agent')->select('id','name','email','address','mobile')->get();
        return response(['agents' => $list], 200);
    }
}
