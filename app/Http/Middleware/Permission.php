<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use App\Models\Rolepermission;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $role_id = Auth::guard('admin')->user()->role_id;
        $url = $request->segment(3);
        $access = RolePermission::where('role_id', $role_id)->where('permission', $url)->count();
        if ($access > 0) {
            return $next($request);
        } else {
            
            return response()->view('errors.permission');
        }
    }
}
