<?php

namespace App\Http\Livewire\Asset;

use Livewire\Component;

class Css extends Component
{
    public function render()
    {
        return view('livewire.asset.css');
    }
}
