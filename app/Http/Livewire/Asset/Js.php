<?php

namespace App\Http\Livewire\Asset;

use Livewire\Component;

class Js extends Component
{
    public function render()
    {
        return view('livewire.asset.js');
    }
}
