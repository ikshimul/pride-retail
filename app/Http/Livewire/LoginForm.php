<?php

namespace App\Http\Livewire;

use Livewire\Component;

class LoginForm extends Component
{
    public $mobile;
    public $password;

    protected $rules = [
        'mobile' => 'required|min:11',
        'password' => 'required',
    ];

    public function submit()
    {
        $this->validate();

        // Execution doesn't reach here if validation fails.

        Contact::create([
            'mobile' => $this->mobile,
            'password' => $this->password,
        ]);
    }
    
    public function render()
    {
        return view('livewire.login-form');
    }
}
