<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Subprocat extends Model
{
    use HasFactory;
    
    public function getall(){
        return DB::table('subprocats')
                ->join('procats', 'procats.id', '=', 'subprocats.procat_id')
                ->select('subprocats.id', 'subprocats.subprocat_name', 'subprocats.subprocat_order', 'subprocats.subprocat_img', 'procats.id as procat_id', 'procats.procat_name')
                ->where('subprocats.status',1)
                ->get();
    }
    
}
