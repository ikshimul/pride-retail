<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
    use HasFactory;
    
    public function adminlist($subpro_id = null){
        if($subpro_id==null){
            $list = DB::table('products')
            ->join('subprocats', 'products.subprocat_id', '=', 'subprocats.id')
            ->join('procats', 'products.procat_id', '=', 'procats.id')
            ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
            ->join('productimgs', 'productimgs.productalbum_id', '=', 'productalbums.id')
            ->select('products.*','productimgs.*', 'subprocats.subprocat_name', 'procats.procat_name')
            ->where('products.trash', 0)
            ->orderBy('products.id', 'DESC')
            ->groupBy('products.id')
            ->get();
        }else{
        $list = DB::table('products')
            ->join('subprocats', 'products.subprocat_id', '=', 'subprocats.id')
            ->join('procats', 'products.procat_id', '=', 'procats.id')
            ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
            ->join('productimgs', 'productimgs.productalbum_id', '=', 'productalbums.id')
            ->select('products.*','productimgs.*', 'subprocats.subprocat_name', 'procats.procat_name')
            ->where('products.subprocat_id', $subpro_id)
            ->where('products.trash', 0)
            ->orderBy('products.id', 'DESC')
            ->groupBy('products.id')
            ->get();
        }
       return $list;
    }
    
}
