<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Signup extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
       $email="$notifiable->email";
       $account="My Account.";
        return (new MailMessage)
                    ->greeting("Dear $notifiable->name")
                    ->line("Welcome to Pride Limited. Your Pride Limited  account has been created.")
                    ->line("Form now on, please login to your account using your email address $email and your password.")
                    ->line("If you need to make any changes to your account or add more information, login to Pride Limited account and click $account")
                    ->action('My Account', route('dashboard'))
                    ->line('Thanks for registering!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}